package qgame.threading;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import qgame.QGame;
import qgame.log.Logger;
import qgame.tools.Pair;

public class SocketWorker implements Runnable {

	private List<Pair<Integer,Runnable>> taskList = new ArrayList<Pair<Integer, Runnable>>();
	private volatile Boolean suspended;
	
	public SocketWorker() {
		suspended = true;
	}
	
	public void addTask(int remoteHash, Runnable task) {
		boolean notify = false;
		synchronized (taskList) {
			taskList.add(new Pair<Integer, Runnable>(remoteHash,task));
			synchronized (suspended) {
				if (suspended) {
					suspended = false;
					notify = true;
				}
			}
			if (notify) {
				synchronized(this) {
					this.notify();
				}
			}
		}
		
	}
	
	public void removeTasks(int remoteHash) {
		synchronized (taskList) {
			for (Iterator<Pair<Integer,Runnable>> iterator = taskList.iterator(); iterator.hasNext();) {
				Pair<Integer,Runnable> entry = iterator.next();
				if (entry.first == remoteHash)
					iterator.remove();
			}
		}
	}
	
	@Override
	public void run() {
		Runnable task;
		while (!QGame.shutdown) {
			while (suspended && !QGame.shutdown) {
				try {
					synchronized (this) {
						wait();
					}
				} catch (InterruptedException e) {
					Logger.debug("SocketWorker got interrupted during wait :");
					Logger.debug(e.toString());
				}
			}
			
			while (true) {
				synchronized (taskList) {
					if (taskList.isEmpty()) {
						suspended = true;
						break;
					}
					task = taskList.remove(0).second;
				}
				task.run();
			}
			
			
		}
	}

}
