package qgame.threading;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class SocketThreadFactory {

	private Map<HashedThread, Integer> referenceMap = new HashMap<HashedThread, Integer>();
	private List<HashedThread> threadList = new ArrayList<HashedThread>();
	private String factoryName;
	private HashedThread nextThread = null;
	private int lowestRefCount = 0;
	
	public SocketThreadFactory(String factoryName) {
		this.factoryName = factoryName;
	}
	
	public String getName() {
		return factoryName;
	}
	
	public void shutdown() {
		for (HashedThread thread : threadList) {
			Runnable r = thread.getTask();
			synchronized (r) {
				r.notify();
			}
		}
	}
	
	public void createThread(Runnable task, String name) {
		HashedThread thread = new HashedThread(task, name);
		threadList.add(thread);
		referenceMap.put(thread, 0);
		nextThread = thread;
		lowestRefCount = 0;
		thread.start();
	}
	
	public HashedThread getNext() {
		HashedThread ret = nextThread;
		findNext();
		return ret;
	}
	
	public void increase(HashedThread thread) {
		int refCount = referenceMap.get(thread);
		referenceMap.put(thread, refCount+1);
		if (thread.equals(nextThread))
			findNext();
	}
	
	public void decrease(HashedThread thread) {
		int refCount = referenceMap.get(thread);
		refCount--;
		referenceMap.put(thread, refCount);
		if (refCount < lowestRefCount) {
			lowestRefCount = refCount;
			nextThread = thread;
		}
	}
	
	private void findNext() {
		for (Entry<HashedThread, Integer> entry : referenceMap.entrySet()) {
			if (entry.getValue() == 0) {
				nextThread = entry.getKey();
				lowestRefCount = 0;
				return;
			}
			if (entry.getValue() < lowestRefCount) {
				nextThread = entry.getKey();
				lowestRefCount = entry.getValue();
			}
		}
	}
	
}
