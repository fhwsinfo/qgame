package qgame.threading;

public class HashedThread extends Thread {

	private static Integer nextId = 0;
	
	private int id;
	private Runnable task;
	
	public HashedThread(Runnable task, String name) {
		super(task, name);
		synchronized (nextId) {
			this.id = nextId;
			nextId++;
		}
		this.task = task;
	}
	
	public Runnable getTask() {
		return task;
	}
	
	@Override
	public int hashCode() {
		return this.getName().hashCode() ^ id;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof HashedThread) {
			return hashCode() == obj.hashCode();
		}
		return false;
	}
	
}
