package qgame.session;

import java.sql.ResultSet;
import java.util.Map;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import qgame.filter.InputFilter;
import qgame.game.menu.MenuItemType;
import qgame.log.Logger;
import qgame.network.netty.WebSocketHandler;
import qgame.network.protocol.Packet;
import qgame.network.protocol.packets.DomReplacePacket;
import qgame.network.protocol.packets.TemplatePacket;
import qgame.network.sql.DatabaseManager;
import qgame.network.sql.DatabaseType;
import qgame.network.sql.Request;

public class SocketSession extends WebSocketHandler {

	private String loginUserName, loginPassword;
	private String registerUserName, registerPassword, registerEmail;
	private int sessionCookie;
	private MenuItemType loginMenu;
	
	public SocketSession(Channel channel) {
		super(channel);
	}

	@Override
	public void handleClientMessage(ChannelHandlerContext ctx, String opcode, Map<String, String> data) {
		String query;
		Request req;
		Packet pck;
		String menuId;
		switch (opcode) {
		case "init":
			TemplatePacket loginTemplate = TemplatePacket.loginTemplatePacket;
			String clientCookie = data.get("session");
			loginUserName = data.get("name");
			menuId = data.get("menuid");
			if (clientCookie == null || loginUserName == null || menuId == null ) {
				String json = loginTemplate.generate();
				ctx.channel().writeAndFlush(new TextWebSocketFrame(json));
				return;
			}
			try {
				loginMenu = MenuItemType.getMenuItemType(Integer.parseInt(menuId));
			} catch (Exception e) {
				String json = loginTemplate.generate();
				ctx.channel().writeAndFlush(new TextWebSocketFrame(json));
				return;
			}
			if (loginMenu == null) {
				String json = loginTemplate.generate();
				ctx.channel().writeAndFlush(new TextWebSocketFrame(json));
				return;
			}
			loginUserName = InputFilter.apply(loginUserName);
			try {
				sessionCookie = Integer.parseInt(clientCookie);
			} catch (NumberFormatException e) {
				String json = loginTemplate.generate();
				ctx.channel().writeAndFlush(new TextWebSocketFrame(json));
				return;
			}
			query = "SELECT * FROM account WHERE login = '"+loginUserName+"';";
			req = new Request(getRemoteHash(),query,DatabaseType.Account,
					(ResultSet result) -> LoginHandler.handleSessionCookie(loginUserName, sessionCookie, loginMenu, result, channel)
					);
			DatabaseManager.sheduleRequest(req);
			break;
		case "formsubmit":
			String formid = data.get("id");
			if (formid == null) {
				Logger.error("inbound form-submit had no id!");
				return;
			}
			if (formid.equals("loginform")) {
				query = "SELECT * FROM account WHERE login = '"+data.get("formname")+"';";
				req = new Request(getRemoteHash(), query, DatabaseType.Account,
						(ResultSet result) -> LoginHandler.handleLogin(loginUserName, loginPassword, channel, result)
						);
				DatabaseManager.sheduleRequest(req);
				loginUserName = InputFilter.apply(data.get("formname"));
				loginPassword = InputFilter.apply(data.get("formpass"));
				return;
			}
			if (formid.equals("registerform")) {
				String pass1 = InputFilter.apply(data.get("registerpass"));
				String pass2 = InputFilter.apply(data.get("registerpass2"));
				if (!pass1.equals(pass2)) {
					pck = new DomReplacePacket("Passw&ouml;rter ungleich!", "loginstatus");
					ctx.writeAndFlush(new TextWebSocketFrame(pck.generate()));
					return;
				}
				if (pass1.length() < 8) {
					pck = new DomReplacePacket("Passwort zu schwach!", "loginstatus");
					ctx.writeAndFlush(new TextWebSocketFrame(pck.generate()));
					return;
				}
				registerUserName = InputFilter.apply(data.get("registername"));
				registerPassword = pass1;
				registerEmail = InputFilter.apply(data.get("registeremail"));
				if (registerUserName.length() < 3) {
					pck = new DomReplacePacket("Username zu kurz!", "loginstatus");
					ctx.writeAndFlush(new TextWebSocketFrame(pck.generate()));
					return;
				}
				
				//TODO additional checks here (password, email etc)
				query = "SELECT * FROM account WHERE login = '"+registerUserName+"';";
				req = new Request(getRemoteHash(), query, DatabaseType.Account, 
						(ResultSet result) -> LoginHandler.handleRegister(registerUserName, registerPassword, registerEmail, channel, result)
						);
				DatabaseManager.sheduleRequest(req);
				
			}
			break;
		default:
			Logger.error("unknown opcode: '"+opcode+"'");
			break;
		}
		
	}
	
}
