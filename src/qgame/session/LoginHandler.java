package qgame.session;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.net.InetSocketAddress;

import qgame.filter.InputFilter;
import qgame.game.menu.MenuItemType;
import qgame.game.permissions.PermissionGroup;
import qgame.log.Logger;
import qgame.network.netty.WebSocketHandler;
import qgame.network.protocol.Packet;
import qgame.network.protocol.packets.DomReplacePacket;
import qgame.network.protocol.packets.SetCookiePacket;
import qgame.network.protocol.packets.TemplatePacket;
import qgame.network.sql.DatabaseManager;
import qgame.network.sql.DatabaseType;
import qgame.network.sql.Request;
import qgame.security.SessionCookie;
import qgame.security.Password;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

public class LoginHandler {

	public static void handleLogin(String user, String pass, Channel ch, ResultSet result) {
		
		String hashedPw = "";
		boolean success = false;
		String dbUser = "";
		int dbUserId = -1;
		
		if (result != null) {
			try {
				if (result.next()) {
					hashedPw = result.getString("password");
					dbUser = result.getString("login");
					dbUserId = result.getInt("id");
					success = Password.check(pass, hashedPw);
				}
			} catch (Exception e) {
				Logger.error("Exception in LoginHandler.handleLogin :");
				Logger.error(e.toString());
				success = false;
			}
		}
		
		if (success) {
			login(dbUserId, dbUser, MenuItemType.Overview, ch);
		} else {
			Packet pck = new DomReplacePacket("Login fehlgeschlagen!", "loginstatus");
			ch.writeAndFlush(new TextWebSocketFrame(pck.generate()));
		}
	}
	
	public static void handleSessionCookie(String user, int cookie, MenuItemType menu, ResultSet result, Channel ch) {
		String json = TemplatePacket.loginTemplatePacket.generate();
		String dbUser;
		int dbUserId;
		if (result == null) {
			ch.writeAndFlush(new TextWebSocketFrame(json));
			return;
		}
		try {
			if (result.next()) {
				int storeCookie = result.getInt("session_cookie");
				dbUser = result.getString("login");
				dbUserId = result.getInt("id");
				if (SessionCookie.checkCookie(cookie, storeCookie, ((InetSocketAddress) ch.remoteAddress()).getAddress().getHostAddress().hashCode())) {
					Logger.info("logging in '"+dbUser+"', session cookie valid");
					login(dbUserId, dbUser, menu, ch);
				} else {
					throw new Exception("");
				}
			} else {
				throw new Exception("");
			}
		} catch (Exception e) {
			ch.writeAndFlush(new TextWebSocketFrame(json));
			return;
		}
	}
	
	private static void login(int userId, String user, MenuItemType menu, Channel ch) {
		PlayerSession pSess = PlayerSession.replaceSession(userId, user, menu, ch);
		WebSocketHandler.get(ch.remoteAddress().hashCode()).setPlayerSession(pSess);
		int randCookie = SessionCookie.generateRandomInt();
		int storeCookie = SessionCookie.generateStoreCookie(randCookie, pSess.getHostAddress().hashCode());
		
		ch.writeAndFlush(new TextWebSocketFrame((new SetCookiePacket("session", String.valueOf(randCookie))).generate()));
		ch.writeAndFlush(new TextWebSocketFrame((new SetCookiePacket("name", user)).generate()));
		
		String ip = ((InetSocketAddress)ch.remoteAddress()).getAddress().getHostAddress();
		String query = "UPDATE account SET last_ip = '"+InputFilter.apply(ip)+"' , last_login = CURRENT_TIMESTAMP , session_cookie = '"+String.valueOf(storeCookie)+"' WHERE login = '"+user+"';";
		Request req = new Request(ch.remoteAddress().hashCode(), query, DatabaseType.Account, (Runnable)null);
		DatabaseManager.sheduleRequest(req);
	}

	public static void handleRegister(String userName, String password, String email, Channel ch, ResultSet result) {
		Request req;
		String query;
		String hashedPw;
		Packet pck;
		if (result == null) {
			pck = new DomReplacePacket("Username nicht verf&uuml;gbar!", "loginstatus");
			ch.writeAndFlush(new TextWebSocketFrame(pck.generate()));
			return;
		}
		try {
			if (result.next()) {
				pck = new DomReplacePacket("Username nicht verf&uuml;gbar!", "loginstatus");
				ch.writeAndFlush(new TextWebSocketFrame(pck.generate()));
				return;
			}
		} catch (Exception e) {
			Logger.error("Exception in LoginHandler.handleRegister :");
			Logger.error(e.toString());
		}
		
		try {
			hashedPw = Password.getSaltedHash(password);
		} catch (Exception e) {
			pck = new DomReplacePacket("Interner Fehler beim Registrierungsorgang!", "loginstatus");
			ch.writeAndFlush(new TextWebSocketFrame(pck.generate()));
			Logger.error("Exception in LoginHandler.handleRegister :");
			Logger.error(e.toString());
			return;
		}
		
		query = "INSERT INTO account (login, password, email) VALUES (";
		query += "'" + userName + "',";
		query += "'" + hashedPw + "',";
		query += "'" + email + "'";
		query += ");";
		
		req = new Request(ch.remoteAddress().hashCode(), query, DatabaseType.Account,
				() -> LoginHandler.finalRegister(userName, password, ch));
		
		DatabaseManager.sheduleRequest(req);
	}
	
	public static void finalRegister(String user, String password, Channel ch) {
		String query = "SELECT * FROM account WHERE login = '"+user+"';";
		Request req = new Request(ch.remoteAddress().hashCode(), query, DatabaseType.Account,
				(ResultSet result) -> LoginHandler.postRegister(user, password, ch, result)
				);
		DatabaseManager.sheduleRequest(req);
	}
	
	public static void postRegister(String user, String password, Channel ch, ResultSet result) {
		if (result == null)
			return;
		try {
			if (result.next()) {
				int userId = result.getInt("id");
				String dbUser = result.getString("login");
				PermissionGroup defaultGroup = PermissionGroup.get("default");
				String query = "REPLACE INTO account_groups (account_id, group_id) VALUES ("+userId+", "+defaultGroup.getId()+");";
				Request req = new Request(ch.remoteAddress().hashCode(), query, DatabaseType.Account,
						() -> login(userId, dbUser, MenuItemType.Overview, ch)
						);
				DatabaseManager.sheduleRequest(req);
			}
		} catch (SQLException e) {
			Logger.sql("SQLException in LoginHandler.postRegister :");
			Logger.sql(e.toString());
		}
	}
	
}
