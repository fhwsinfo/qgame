package qgame.session;

import java.net.InetSocketAddress;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import qgame.console.ConsoleOutputBuffer;
import qgame.console.command.CommandHandler;
import qgame.filter.ChatFilter;
import qgame.filter.InputFilter;
import qgame.filter.UrlFinder;
import qgame.game.chat.ChatBuffer;
import qgame.game.chat.ChatMessage;
import qgame.game.chat.ChatType;
import qgame.game.menu.MenuItemType;
import qgame.game.menu.MenuSectionType;
import qgame.game.menu.PlayerMenu;
import qgame.game.permissions.PermissionGroup;
import qgame.log.LogLevel;
import qgame.log.Logger;
import qgame.network.netty.WebSocketHandler;
import qgame.network.protocol.DomManipulationOpcode;
import qgame.network.protocol.Packet;
import qgame.network.protocol.TemplateOpcode;
import qgame.network.protocol.TemplateType;
import qgame.network.protocol.packets.DomCopyPacket;
import qgame.network.protocol.packets.SetCookiePacket;
import qgame.network.protocol.packets.SimpleDomManipulationPacket;
import qgame.network.protocol.packets.TemplatePacket;
import qgame.network.protocol.packets.menu.MenuItemPacket;
import qgame.network.protocol.packets.menu.MenuSectionPacket;
import qgame.network.sql.DatabaseManager;
import qgame.network.sql.DatabaseType;
import qgame.network.sql.Request;
import qgame.tools.Debugger;
import qgame.tools.Time;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

public class PlayerSession extends PermissionGroup {

	private static final Map<String /* userName */, PlayerSession> sessions = new HashMap<String, PlayerSession>();
	
	public static PlayerSession getSession(String user) {
		PlayerSession pSess = sessions.get(user.toLowerCase());
		return pSess;
	}
	
	public static PlayerSession getSession(int userId, String userName, Channel ch) {
		PlayerSession pSess = sessions.get(userName.toLowerCase());
		if (pSess != null)
			return pSess;
		return new PlayerSession(userId, userName, MenuItemType.Overview, ch);
	}
	
	public static PlayerSession replaceSession(int userId, String user, MenuItemType menu, Channel ch) {
		PlayerSession pSess = sessions.get(user.toLowerCase());
		if (pSess != null) {
			pSess.destroy();
		}
		return new PlayerSession(userId, user, menu, ch);
	}
	
	public static void requestOfflinePlayer(String userName) {
		String query = "SELECT * FROM account WHERE login='"+userName+"';";
		Request req = new Request(null, query, DatabaseType.Account, (ResultSet result) -> PlayerSession.handleOfflinePlayer(result));
		DatabaseManager.sheduleRequest(req);
	}
	
	public static void handleOfflinePlayer(ResultSet result) {
		String userName;
		int userId;
		try {
			while (result.next()) {
				userName = result.getString("login");
				userId = result.getInt("id");
				PlayerSession pSess = PlayerSession.getSession(userName);
				if (pSess == null) {
					replaceSession(userId, userName, null, null);
				}
			}
		} catch (Exception e) {
			Logger.error("Exception in PlayerSession.handleOfflinePlayer :");
			Logger.error(e.toString());
		}
	}
	
	public static boolean exists(String user) {
		return sessions.containsKey(user.toLowerCase());
	}
	
	public static void broadcast(Packet pck) {
		String msg = pck.generate();
		for (PlayerSession pSess : sessions.values())
			pSess.sendRaw(msg);
	}
	
	public static void broadcast(ChatMessage addMessage, ChatMessage removeMessage) {
		for (PlayerSession pSess : sessions.values()) {
			pSess.sendChatMessage(addMessage);
			if (removeMessage != null)
				pSess.removeChatMessage(removeMessage);
		}
	}
	
	public static void destroyAllSessions() {
		for (PlayerSession pSess : sessions.values())
			pSess.destroy();
	}
	
	
	
	private int userId;
	private String user;
	private Channel channel;
	private ChatType chatType;
	private MenuItemType startingMenu;
	private List<Integer> chatMessageList = new ArrayList<Integer>();
	private List<PermissionGroup> permissionGroups = new ArrayList<PermissionGroup>();
	private boolean isOnline;
	private int init;
	private PlayerMenu menu;
	
	private PlayerSession(int userId, String user, MenuItemType menu, Channel ch) {
		super(userId, null,ch == null ? null : ch.remoteAddress().hashCode());
		if (ch == null)
			Logger.debug("Creating offline user '"+user+"'...");
		else
			Logger.debug("Creating online user '"+user+"'...");
		this.userId = userId;
		this.user = user;
		this.channel = ch;
		this.init = 0;
		this.startingMenu = menu;
		this.chatType = ChatType.User;
		sessions.put(user.toLowerCase(), this);
		isOnline = false;
		this.menu = new PlayerMenu(this);
		String query;
		Request req;
		query = "SELECT * FROM account_groups WHERE `account_id` = "+userId+";";
		req = new Request(ch == null ? null : ch.remoteAddress().hashCode(), query, DatabaseType.Account,
				(ResultSet result) -> setPermissionGroups(result)
				);
		DatabaseManager.sheduleRequest(req);
	}
	
	private PlayerSession( int userId, String user ) {
		this(userId, user, null, null);
	}
	
	
	
	public boolean isOnlineSession() {
		return channel != null;
	}
	
	@Override 
	protected void setPermissions(ResultSet result) {
		super.setPermissions(result);
		init++;
		readyCheck();
	}
	
	@Override
	public String getPermission(String perm)  {
		String ret = super.getPermission(perm);
		String test;
		int priority  = -1;
		if (ret != null)
			return ret;
		for (PermissionGroup group : permissionGroups) {
			if (group.getPriority() > priority) {
				test = group.getPermission(perm);
				if (test != null) {
					ret = test;
					priority = group.getPriority();
				}
			}
		}
		return ret;
	}
	
	@Override
	public void setPermission(String perm, String value, boolean silent) {
		perm = perm.toUpperCase();
		value = value.toLowerCase();
		permissions.put(perm, value);
		if (silent)
			return;
		String test = permissions.get(perm);
		if (test.equals(value))
			return;
		String query = "REPLACE INTO permissions (account_id, permission, permission_value) VALUES ("+userId+", '"+perm+"', '"+value+"');";
		Request req = new Request(channel.remoteAddress().hashCode(), query, DatabaseType.Account, (Runnable) null);
		DatabaseManager.sheduleRequest(req);
	}
	
	private void setPermissionGroups(ResultSet result) {
		int id;
		PermissionGroup group;
		try {
			while (result.next()) {
				id = result.getInt("group_id");
				group = PermissionGroup.get(id);
				if (group != null)
					permissionGroups.add(group);
			}
		} catch (SQLException e) {
			Logger.sql("SQLException in PlayerSession.setPermissionGroups :");
			Logger.sql(e.toString());
		}
		
		init++;
		readyCheck();
	}
	
	public void setPermissionGroup(PermissionGroup group) {
		if (group == null)
			return;
		if (permissionGroups.contains(group))
			return;
		permissionGroups.add(group);
		String query = "REPLACE INTO account_groups (account_id, group_id) VALUES ("+userId+", "+group.getId()+");";
		Request req = new Request(channel == null ? null : channel.remoteAddress().hashCode(), query, DatabaseType.Account, (Runnable) null);
		DatabaseManager.sheduleRequest(req);
	}
	
	public void removePermissionGroup(PermissionGroup group) {
		if (group == null)
			return;
		if (!permissionGroups.remove(group))
			return;
		String query = "DELETE FROM account_groups WHERE `account_id` = "+userId+" AND `group_id` = "+group.getId()+";";
		Request req = new Request(channel.remoteAddress().hashCode(), query, DatabaseType.Account, (Runnable) null);
		DatabaseManager.sheduleRequest(req);
	}
	
	public boolean hasPermission(String perm) {
		String check;
		if ((check = getPermission(perm)) != null)
			if (check.trim().equals("1"))
				return true;
		return false;
	}
	
	private void readyCheck() {
		if (init == 2) {
			if (channel == null) 
				Logger.debug("Offline user '"+user+"' created.");
			else
				Logger.debug("Online user '"+user+"' created.");
			PermissionGroup defaultGroup = PermissionGroup.get("default");
			if (!permissionGroups.contains(defaultGroup))
				this.setPermissionGroup(defaultGroup);
			
			chatType = ChatType.getChatType(this.getPermission("CHAT:TYPE"));
			
			if (channel != null) {
				isOnline = true;
				sendGameLayout();
				PlayerMenu.sendMenu(this);
				if (hasPermission("MENU:DISPLAY_ADMIN_SECTION")) {
					send(new MenuSectionPacket(MenuSectionType.Admin));
					send(new MenuItemPacket(MenuItemType.Debug));
					send(new MenuItemPacket(MenuItemType.Console));
				}
						
				getMenu().switchMenu(startingMenu);
				ChatBuffer.send(this);
			} else {
				ChatBuffer.setChatType(user, chatType);
			}
		}
	}
	
	public String getHostAddress() {
		return ((InetSocketAddress) channel.remoteAddress()).getAddress().getHostAddress();
	}
	
	public int getUserId() {
		return userId;
	}
	
	public String getUsername() {
		return user;
	}
	
	public PlayerMenu getMenu() {
		return menu;
	}
	
	public void send(Packet pck) {
		if (isOnline)
			channel.writeAndFlush(new TextWebSocketFrame(pck.generate()));
	}
	
	private void sendRaw(String msg) {
		if (isOnline)
			channel.writeAndFlush(new TextWebSocketFrame(msg));
	}
	
	public void sendChatMessage(ChatMessage msg) {
		if (!isOnline)
			return;
		final int msg_id = msg.getId();
		boolean send = false;
		synchronized (chatMessageList) {
			if (!chatMessageList.contains(msg_id)) {
				chatMessageList.add(msg_id);
				send = true;
			}
		}
		//TODO: Permission f�r Anzeigen vom Bild (OWN / OTHER)
		
		if (send)
			send(msg.generatePacket());
	}
	
	public void removeChatMessage(ChatMessage msg) {
		if (!isOnline)
			return;
		final int msg_id = msg.getId();
		boolean remove = false;
		synchronized (chatMessageList) {
			if (chatMessageList.contains(msg_id)) {
				chatMessageList.remove((Integer) msg_id);
				remove = true;
			}
		}
		if (remove)
			send(new SimpleDomManipulationPacket(DomManipulationOpcode.Remove, null, "chatbox_text_"+msg_id));
	}
	
	public void destroy() {
		if (!isOnline)
			return;
		ConsoleOutputBuffer.removeListener(this);
		WebSocketHandler.remove(channel.remoteAddress().hashCode());
		DatabaseManager.cancelRequests(channel.remoteAddress().hashCode());
		channel = null;
		isOnline = false;
	}
	
	
	
	public void handleInbound(Map<String, String> data) {
		if (!isOnline)
			return;
		String opcode = data.get("opcode");
		if (opcode == null) {
			handleBadInbound(data);
			return;
		}
		opcode = opcode.trim().toLowerCase();
		int i;
		switch (opcode) {
			case "formsubmit":
				handleFormInbound(data);
				break;
			case "switchmenu":
				try {
					i = Integer.parseInt(data.get("target").trim());
					menu.switchMenu(MenuItemType.getMenuItemType(i)); // Permission check happens in PlayerMenu.class
				} catch (Exception e) {}
			default:
				break;
		}
			
	}
	
	private void handleFormInbound(Map<String, String> data) {
		String formid = data.get("id");
		if (formid == null) {
			handleBadInbound(data);
			return;
		}
		switch (formid.toLowerCase()) {
			case "chatform":
				String msg = data.get("chatbox_input");
				if (msg == null) {
					handleBadInbound(data);
					return;
				}
				msg = InputFilter.apply(msg.trim()).trim();
				if (msg.equals(""))
					return;
				if (msg.length() > 500) {
					msg = msg.substring(0, 500);
				}
				if (msg.startsWith("/")) {
					String result = CommandHandler.handleCommand(msg.replaceFirst("/", ""), this);
					if (result != "") {
						//TODO send system message
						break;
					}
				}
				ChatMessage addMessage = new ChatMessage(userId, user, UrlFinder.parseUrls(ChatFilter.apply(msg)), Time.currentTimeString(), chatType);
				addMessage.log();
				broadcast(addMessage, ChatBuffer.addMessage(addMessage));
				break;
			default:
				handleBadInbound(data);
				return;
		}
	}
	
	private void handleBadInbound(Map<String, String> data) {
		Logger.debug("bad inbound from "+user);
		Debugger.Debug(data);
	}
	
	
	
	public void sendGameLayout() {
		if (isOnline)
			send(TemplatePacket.gameLayoutTemplatePacket);
	}
	
	
	
	public void setCookie(String name, String value) {
		if (isOnline)
			this.send(new SetCookiePacket(name, value));
	}

	public void onMenuSwitch(MenuItemType source, MenuItemType target) {
		if (source != null)
			if (source == MenuItemType.Console)
				ConsoleOutputBuffer.removeListener(this);
		switch (target) {
			case Debug:
				send(new DomCopyPacket("debug", "contentbox"));
				break;
			case Console:
				send(new TemplatePacket(TemplateOpcode.Replace,"template_content."+target.template, "contentbox"));
				TemplatePacket consoleCheckboxTemplate = new TemplatePacket(TemplateType.Console_Checkbox);
				for (LogLevel logLevel : LogLevel.values()) {
					consoleCheckboxTemplate.put("$title$", logLevel.name());
					consoleCheckboxTemplate.put("$logtype$", logLevel.logType);
					consoleCheckboxTemplate.put("$script$", "setCssValue('style_"+logLevel.logType+"', '#console_messages ."+logLevel.logType+"', !this.checked, 'display', 'none');");
					send(consoleCheckboxTemplate);
				}
				ConsoleOutputBuffer.addListener(this);
				break;
			default:
				send(new TemplatePacket(TemplateOpcode.Replace,"template_content."+target.template, "contentbox"));
				break;
		}
	}
	
}
