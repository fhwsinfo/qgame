package qgame.network.netty;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.websocketx.CloseWebSocketFrame;
import io.netty.handler.codec.http.websocketx.PingWebSocketFrame;
import io.netty.handler.codec.http.websocketx.PongWebSocketFrame;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshaker;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshakerFactory;
import io.netty.util.CharsetUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.json.simple.parser.ParseException;

import qgame.QGame;
import qgame.filter.JsonFilter;
import qgame.log.Logger;
import qgame.session.PlayerSession;
import qgame.threading.HashedThread;
import qgame.threading.SocketWorker;
import qgame.tools.JSON;
import qgame.tools.Pair;
import static io.netty.handler.codec.http.HttpHeaders.Names.*;
import static io.netty.handler.codec.http.HttpHeaders.*;
import static io.netty.handler.codec.http.HttpMethod.*;
import static io.netty.handler.codec.http.HttpResponseStatus.*;
import static io.netty.handler.codec.http.HttpVersion.*;

/**
 * 
 * Manages a connection to a client and handles inbound Messages.
 * 
 * @author Kagamul
 * 
 */
public abstract class WebSocketHandler extends SimpleChannelInboundHandler<Object> {
	
	/**
	 * maps the Hash of the RemoteAddress (unique to each Socket) to the related WebSocketHandler instance.
	 */
	private static Map<Integer /* remoteHash */, WebSocketHandler> socketHandlerMap = new HashMap<Integer, WebSocketHandler>();
	
	private static final ScheduledExecutorService keepAlive = Executors.newScheduledThreadPool(1);
	
	public static WebSocketHandler get(int remoteHash) {
		return socketHandlerMap.get(remoteHash);
	}
	
	public static void shutdown() {
		keepAlive.shutdownNow();
	}
	
	public static void remove(int remoteHash) {
		WebSocketHandler socketHandler = socketHandlerMap.get(remoteHash);
		if (socketHandler != null) {
			socketHandler.destroy();
			socketHandlerMap.remove(remoteHash);
		}
	}
	
	public static void destroyAllSockets() {
		for (WebSocketHandler socket : socketHandlerMap.values())
			socket.destroy();
	}
	
	private Map<Integer /* message id */, Pair<ChannelHandlerContext, Object /* message */>> messageMap = new HashMap<Integer, Pair<ChannelHandlerContext, Object>>();
	private Integer currentMessageId = 0;
	
	private WebSocketServerHandshaker handshaker;
	
	private PlayerSession pSess = null;
	protected Channel channel = null;
	private int remoteHash = 0;
	
	private SocketWorker worker;
	private HashedThread thread;
	
	private long lastPing;

	public WebSocketHandler(Channel channel) {
		this.channel = channel;
		this.remoteHash = channel.remoteAddress().hashCode();
		socketHandlerMap.put(channel.remoteAddress().hashCode(), this);
		lastPing = 0;
		keepAlive.schedule(() -> sendPing(), 3000, TimeUnit.MILLISECONDS);
		thread = QGame.getSocketThreadFactory().getNext();
		worker = (SocketWorker) thread.getTask();
	}
	
	public SocketWorker getWorker() {
		return worker;
	}
	
	public void sendPing() {
		if (channel.isActive()) {
			lastPing = System.currentTimeMillis();
			channel.writeAndFlush(new TextWebSocketFrame("{\"ping\":\""+lastPing+"\"}"));
		}
	}
	
	public int getRemoteHash() {
		return remoteHash;
	}
	
	public void setPlayerSession(PlayerSession pSess) {
		this.pSess = pSess;
	}
	
	public PlayerSession getPlayerSession() {
		return pSess;
	}
	
	@Override
	public void messageReceived(ChannelHandlerContext ctx, Object msg) throws Exception {
		int id = currentMessageId;
		if (msg instanceof FullHttpRequest) {
			synchronized (messageMap) {
				messageMap.put(id, new Pair<ChannelHandlerContext, Object>(ctx, ((FullHttpRequest)msg).copy()));
			}
			currentMessageId++;
			worker.addTask(remoteHash, () -> handleHttpRequest(id));
		} else if (msg instanceof WebSocketFrame) {
			synchronized (messageMap) {
				messageMap.put(id, new Pair<ChannelHandlerContext, Object>(ctx, ((WebSocketFrame)msg).copy()));
			}
			currentMessageId++;
			worker.addTask(remoteHash, () -> handleWebSocketFrame(id));
		}
	}

	private void handleHttpRequest(int messageId) {
		FullHttpRequest req;
		ChannelHandlerContext ctx;
		Pair<ChannelHandlerContext, Object> messageMapEntry;
		synchronized (messageMap) {
			messageMapEntry = messageMap.remove(messageId);
			ctx = messageMapEntry.first;
			req = (FullHttpRequest) messageMapEntry.second;
		}
		// Handle a bad request.
		if (!req.getDecoderResult().isSuccess()) {
			sendHttpResponse(ctx, req, new DefaultFullHttpResponse(HTTP_1_1, BAD_REQUEST));
			req.release();
			return;
		}
		
		if (req.getMethod() != GET) {
			sendHttpResponse(ctx, req, new DefaultFullHttpResponse(HTTP_1_1, FORBIDDEN));
			req.release();
			return;
		}
		
		String upgrade = req.headers().get(UPGRADE);
		if (upgrade == null) {
			Logger.debug("Unhandled http request:");
			Logger.debug(req.toString());
			Logger.debug(ctx.channel().remoteAddress().toString());
			req.release();
			return;
		} else {
			if (!upgrade.equalsIgnoreCase("websocket")) {
				sendHttpResponse(ctx, req, new DefaultFullHttpResponse(HTTP_1_1, FORBIDDEN));
				req.release();
				return;
			}
		}
		
		// Handshake
		WebSocketServerHandshakerFactory wsFactory = new WebSocketServerHandshakerFactory("wss://"+req.headers().get(HOST)+"/", null, false);
		handshaker = wsFactory.newHandshaker(req);
		if (handshaker == null) {
			WebSocketServerHandshakerFactory
					.sendUnsupportedWebSocketVersionResponse(ctx.channel());
		} else {
			handshaker.handshake(ctx.channel(), req);
		}
	}
	
	private static void sendHttpResponse(ChannelHandlerContext ctx, FullHttpRequest req, FullHttpResponse res) {
		// Generate an error page if response getStatus code is not OK (200).
		if (res.getStatus().code() != 200) {
			ByteBuf buf = Unpooled.copiedBuffer(res.getStatus().toString(),
					CharsetUtil.UTF_8);
			res.content().writeBytes(buf);
			buf.release();
			setContentLength(res, res.content().readableBytes());
		}
		// Send the response and close the connection if necessary.
		ChannelFuture f = ctx.channel().writeAndFlush(res);
		if (!isKeepAlive(req) || res.getStatus().code() != 200) {
			f.addListener(ChannelFutureListener.CLOSE);
		}
	}

	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
		worker.addTask(remoteHash, () -> ctx.flush());
	}

	private void handleWebSocketFrame(int messageId) {
		WebSocketFrame frame;
		ChannelHandlerContext ctx;
		Pair<ChannelHandlerContext, Object> messageMapEntry;
		synchronized (messageMap) {
			messageMapEntry = messageMap.remove(messageId);
			ctx = messageMapEntry.first;
			frame = (WebSocketFrame) messageMapEntry.second;
		}
		long ping;
		// Check for closing frame
		if (frame instanceof CloseWebSocketFrame) {
			handshaker.close(ctx.channel(),
					(CloseWebSocketFrame) frame.retain());
			if (pSess != null) {
				pSess.destroy();
			}
			frame.release();
			return;
		}
		if (frame instanceof PingWebSocketFrame) {
			ctx.channel().writeAndFlush(new PongWebSocketFrame(frame.content().retain()));
			if (pSess != null)
				Logger.debug(pSess.getUsername()+": PING!");
			frame.release();
			return;
		}
		if (frame instanceof PongWebSocketFrame) {
			ctx.channel().writeAndFlush(new PingWebSocketFrame(frame.content().retain()));
			if (pSess != null)
				Logger.debug(pSess.getUsername()+" PONG!");
			frame.release();
			return;
		}
		if (!(frame instanceof TextWebSocketFrame)) {
			throw new UnsupportedOperationException(String.format(
					"%s frame types not supported", frame.getClass().getName()));
		}
		
		String request = ((TextWebSocketFrame) frame).text();
		frame.release();
		//Logger.debug(String.format("%s received %s", ctx.channel(), request)); //TODO write to log file but not to chat/console etc
		try {
			Map<String, String> data = JSON.parseJson(JsonFilter.apply(request));
			
			if (data.containsKey("ping")) {
				try {
					ping = Long.parseLong(data.get("ping"));
					if (ping != lastPing)
						throw new Exception("Bad Ping!");
					lastPing = System.currentTimeMillis();
					if ((lastPing - ping) > 5000)
						sendPing();
					else
						keepAlive.schedule(() -> sendPing(), 5000 - lastPing + ping, TimeUnit.MILLISECONDS);
				} catch (Exception e) {
					try {
						exceptionCaught(ctx, e);
					} catch (Exception e1) {
						Logger.error("Exeption in WebSocketServer.handleWebSocketFrame :");
						Logger.error(e1.toString());
					}
				}
				return;
			}
			
			if (pSess != null) {
				pSess.handleInbound(data);
				return;
			}
			String opcode = data.get("opcode");
			if (opcode == null) {
				Logger.error("inbound json request had no opcode!");
				return;
			}
			
			handleClientMessage(ctx, opcode, data);
			
		} catch (ParseException e) {
			Logger.error("failed to parse inbound json request");
			return;
		}
		
	}
	
	public abstract void handleClientMessage(ChannelHandlerContext ctx, String opcode, Map<String, String> data);

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		if (pSess != null) {
			pSess.destroy();
			Logger.debug("Session "+pSess.getUsername()+" destroyed, exception caught:");
		} else {
			Logger.debug("Connection destroyed, exception caught:");
		}
		if (cause instanceof IOException) {
			IOException ioe = (IOException) cause;
			Logger.debug(ioe.getLocalizedMessage());
		} else {
			Logger.debug(cause.toString());
		}
		ctx.close();
	}
	
	public void destroy() {
		channel.close();
	}
	
}