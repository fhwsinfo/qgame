package qgame.network.netty;

import qgame.session.SocketSession;

import javax.net.ssl.SSLEngine;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.ssl.SslHandler;
import io.netty.handler.timeout.ReadTimeoutHandler;

/**
 * 
 * An Initializer class for newly established Connections to a WebSocketServer.
 * It defines how inbound data is going to be handled. 
 * 
 * @author Kagamul
 *
 */
public class WebSocketInitializer extends ChannelInitializer<SocketChannel> {
	
	@Override
	public void initChannel(SocketChannel ch) throws Exception {
		ChannelPipeline pipeline = ch.pipeline();
		
		SSLEngine engine = WebSocketServerSslContext.getInstance().serverContext().createSSLEngine();
		engine.setUseClientMode(false);
		pipeline.addLast("ssl", new SslHandler(engine));
		
		pipeline.addLast("timeout", new ReadTimeoutHandler(20));
		pipeline.addLast("codec-http", new HttpServerCodec());
		pipeline.addLast("aggregator", new HttpObjectAggregator(65536));
		pipeline.addLast("handler", new SocketSession(ch));
	}
	
}
