package qgame.network.netty;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;

import qgame.log.Logger;

import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.Security;

/**
 * Creates a {@link SSLContext} for just server certificates.
 */
public final class WebSocketServerSslContext {

    private static final String PROTOCOL = "TLS";
    private final SSLContext _serverContext;

    /**
     * Returns the singleton instance for this class
     */
    public static WebSocketServerSslContext getInstance() {
        return SingletonHolder.INSTANCE;
    }

    /**
     * SingletonHolder is loaded on the first execution of Singleton.getInstance() or the first access to
     * SingletonHolder.INSTANCE, not before.
     *
     * See http://en.wikipedia.org/wiki/Singleton_pattern
     */
    private interface SingletonHolder {
    	WebSocketServerSslContext INSTANCE = new WebSocketServerSslContext();
    }

    /**
     * Constructor for singleton
     */
    private WebSocketServerSslContext() {
        SSLContext serverContext = null;
        try {
            // Key store (Server side certificate)
            String algorithm = Security.getProperty("ssl.KeyManagerFactory.algorithm");
            if (algorithm == null) {
                algorithm = "SunX509";
            }

            try {
                String certificatePath = "/etc/ssl/localcerts/apache.pfx";//QGame.getKeystore_path();
                InputStream is = new FileInputStream(certificatePath);
                
                KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
                KeyStore ks = KeyStore.getInstance("pkcs12", "SunJSSE"); 
                ks.load(is,"test".toCharArray());
                
                kmf.init(ks, "test".toCharArray());
                
                // Initialise the SSLContext to work with our key managers.
                serverContext = SSLContext.getInstance(PROTOCOL);
                serverContext.init(kmf.getKeyManagers(), null, null);
            } catch (Exception e) {
            	Logger.error("Failed to initialize the server-side SSLContext:");
            	Logger.error(e.toString());
            }
        } catch (Exception ex) {
            Logger.error("Error initializing SslContextManager:");
            Logger.error(ex.toString());
            System.exit(1);
        } finally {
            _serverContext = serverContext;
        }
    }

    /**
     * Returns the server context with server side key store
     */
    public SSLContext serverContext() {
        return _serverContext;
    }

}