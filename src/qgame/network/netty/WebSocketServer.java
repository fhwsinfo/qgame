package qgame.network.netty;

import qgame.log.Logger;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * 
 * The WebSocketServer.
 * It listens on the specified port for incoming connections.
 * 
 * @author Kagamul
 *
 */
public class WebSocketServer {
	private final int port;
	private Channel ch;

	public WebSocketServer(int port) {
		this.port = port;
		ch = null;
	}
	
	public void close() {
		ch.disconnect();
	}

	public void run() throws Exception {
		EventLoopGroup bossGroup = new NioEventLoopGroup();
		EventLoopGroup workerGroup = new NioEventLoopGroup();
		try {
			ServerBootstrap b = new ServerBootstrap();
			b.group(bossGroup, workerGroup)
					.channel(NioServerSocketChannel.class)
					.childHandler(new WebSocketInitializer());
			ch = b.bind(port).sync().channel();
			Logger.info("Web socket server started on port " + port + '.');
			ch.closeFuture().sync();
		} finally {
			bossGroup.shutdownGracefully();
			workerGroup.shutdownGracefully();
		}
	}

}
