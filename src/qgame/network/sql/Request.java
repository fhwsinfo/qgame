package qgame.network.sql;

import java.sql.ResultSet;

import qgame.network.netty.WebSocketHandler;

public class Request {

	private Runnable executeCallback = null;
	private DatabaseResultSetCallback returnCallback = null;
	private String query;
	private QueryType qType;
	private DatabaseType dbType;
	private Integer remoteHash;
	
	public Request(Integer remoteHash, String query, DatabaseType dbType, Runnable r) {
		this.executeCallback = r;
		this.query = query;
		this.qType = QueryType.Execute;
		this.dbType = dbType;
		this.remoteHash = remoteHash;
	}
	
	public Request(Integer remoteHash, String query, DatabaseType dbType, DatabaseResultSetCallback r) {
		this.returnCallback = r;
		this.query = query;
		this.qType = QueryType.Return;
		this.dbType = dbType;
		this.remoteHash = remoteHash;
	}
	
	public String getQuery() {
		return query;
	}
	
	public QueryType getQueryType() {
		return qType;
	}
	
	public DatabaseType getDatabaseType() {
		return dbType;
	}
	
	public Integer getRemoteHash() {
		return remoteHash;
	}
	
	public void callback(ResultSet result) {
		if (qType == QueryType.Execute) {
			if (remoteHash == null) {
				if (executeCallback != null)
					executeCallback.run();
			} else
				if (executeCallback != null)
					WebSocketHandler.get(remoteHash).getWorker().addTask(remoteHash, executeCallback);
		}
		if (qType == QueryType.Return) {
			if (remoteHash == null) {
				if (returnCallback != null)
					returnCallback.run(result);
			} else
				if (returnCallback != null)
					WebSocketHandler.get(remoteHash).getWorker().addTask(remoteHash, () -> returnCallback.run(result));
		}
	}
	
}
