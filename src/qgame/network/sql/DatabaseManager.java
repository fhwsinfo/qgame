package qgame.network.sql;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import qgame.QGame;
import qgame.log.Logger;

public class DatabaseManager {

	private static String host;
	private static int port;
	private static String user;
	private static String password;
	
	private static boolean shutdown = false;
	private static boolean ready = false;
	private static Boolean hasWork = true;
	
	private static MySQLDatabase accountDatabase;
	private static MySQLDatabase gameDatabase;
	private static MySQLDatabase logDatabase;
	
	private static List<Request> requests = Collections.synchronizedList(new ArrayList<Request>());
	
	public static void initialize() {
		synchronized (QGame.mainConfig) {
			host = QGame.mainConfig.get("mysql_host", "localhost", (String str) -> str != null && !str.isEmpty());
			port = QGame.mainConfig.get("mysql_port", 3306, 1, 49151);
			user = QGame.mainConfig.get("mysql_user", "root", (String str) -> str != null &&!str.isEmpty());
			password = QGame.mainConfig.get("mysql_password", "", (String str) -> str != null);
		}
		accountDatabase = new MySQLDatabase(host, port, user, password, "account");
		gameDatabase = new MySQLDatabase(host, port, user, password, "game");
		logDatabase = new MySQLDatabase(host, port, user, password, "log");
	}
	
	public static void sheduleRequest(Request req) {
		requests.add(req);
		synchronized(DatabaseManager.class) {
			if (!hasWork) {
				DatabaseManager.class.notify();
			}
		}
			
	}
	
	public static void cancelRequests(int remoteHash) {
		synchronized (requests) {
			for (Iterator<Request> it = requests.iterator(); it.hasNext();) {
				Request req = it.next();
				if (req.getRemoteHash() != null && req.getRemoteHash() == remoteHash) {
					it.remove();
				}
			}
		}
	}
	
	public static boolean isReady() {
		return ready;
	}
	
	public static void workLoop() {
		
		Request req = null;
		DatabaseType dbType;
		QueryType qType;
		ResultSet result;
		MySQLDatabase db = null;
		ready = true;
		Logger.info("DatabaseManager started!");
		
		boolean wait, success;
		
		while (!shutdown) {
			
			synchronized (DatabaseManager.class) {
				wait = false;
				synchronized (requests) {
					if (!requests.isEmpty()) {
						req = requests.remove(0);
						synchronized (DatabaseManager.class) {
							hasWork = true;
						}
					} else {
						wait = true;
					}
				}
				if (wait) {
					hasWork = false;
					try {
						DatabaseManager.class.wait();
					} catch (InterruptedException e) {
						Logger.debug("DatabaseManager got interrupted during wait!");
					}
				}
			}
			
			if (hasWork) {
				dbType = req.getDatabaseType();
				qType = req.getQueryType();
				switch (dbType) {
				case Account:
					db = accountDatabase;
					break;
				case Game:
					db = gameDatabase;
					break;
				case Log:
					db = logDatabase;
				}
				switch (qType) {
				case Execute:
					success = db.RunQuery(req.getQuery());
					if (!success) {
						synchronized (requests) {
							requests.add(req);
						}
						break;
					}
					req.callback(null);
					break;
				case Return:
					result = db.ReturnQuery(req.getQuery());
					if (result == null) {
						synchronized (requests) {
							requests.add(req);
						}
						break;
					}
					req.callback(result);
					break;
				}
			}
		}
	}
	
	public static void shutdown() {
		shutdown = true;
	}
	
}
