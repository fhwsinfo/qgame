package qgame.network.sql;

import java.sql.ResultSet;

public interface DatabaseResultSetCallback {

	public void run(ResultSet result);
	
}
