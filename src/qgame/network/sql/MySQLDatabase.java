package qgame.network.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import qgame.log.Logger;

public class MySQLDatabase {
	private String		Username;
	private String		Password;
	private String		Driver		= "com.mysql.jdbc.Driver";
	private String		URL;//			= "jdbc:mysql://127.0.0.1:3306/VanillaAuth";
	private Connection	connection;
	
	private boolean connected;
	private boolean debug;

	/*
	 * Soldwate der Kondawdstruktur ohne argumente aufgerufen werden, werden die
	 * in der klasse genutzten Werte genommen.
	 */

	public MySQLDatabase(String host, int port, String user, String pass, String Database) {
		this.Username = user;
		this.Password = pass;
		this.URL = "jdbc:mysql://"+host+":"+Integer.toString(port)+"/"+Database;
		this.connected = false;
		this.debug = true;
		this.Connect();
	}
	
	public MySQLDatabase(String user, String pass, String Database) {
		this("127.0.0.1",3306,user,pass,Database);
	}
	
	public MySQLDatabase(String host, String user, String pass, String Database) {
		this(host,3306,user,pass,Database);
	}
	
	public MySQLDatabase(int port, String user, String pass, String Database) {
		this("127.0.0.1",port,user,pass,Database);
	}

	/*
	 * Sollte der Konstruktur mit den Argumenten user und pass aufgerufen
	 * werden, werden diese definiert und dann Verbunden.
	 */

	public void Close() {
		if (this.connection != null) {
			try {
				this.connection.close();
				this.connected = false;
				Logger.sql("Connection closed: "+URL);
			} catch (Exception e) {
				Logger.sql("Execption while closing connection: "+URL+" :");
				Logger.sql(e.toString());
			}
		}
	}

	/*
	 * Die Funktion Close() schlie�t das Query um den Speicher wieder frei zu
	 * geben
	 */

	public void Connect() {
		try {
			Class.forName(this.Driver);
			this.connection = DriverManager.getConnection(this.URL, this.Username, this.Password);
			this.connected = true;
			Logger.sql("Connected to Database '"+URL+"'.");
			debug = true;
		} catch (Exception e) {
			if (debug) {
				Logger.sql("Couldnt connect to Database '"+URL+"' :");
				Logger.sql(e.getMessage());
				debug = false;
			}
			this.connected = false;
		}
	}

	/*
	 * Connect registriert den JDBC Treiber und versucht eine Verbindung
	 * herzustellen. Sollte dies nicht m�glich sein, wird eine Exception
	 * ausgel�st
	 */

	public boolean isConnected() {
		if (!connected)
			return false;
		try {
			ResultSet rs = this.ReturnQuery("SELECT 1;");
			if (rs == null) {
				return false;
			}
			if (rs.next()) {
				return true;
			}
			return false;
		} catch (Exception e) {
			connected = false;
			return false;
		}
	}

	/*
	 * fr�gt ein einfaches Query ab, welches "1" zur�ck liefert, falls man
	 * verbunden ist
	 */

	public ResultSet ReturnQuery(String query) {
		if (!connected) {
			Connect();
			if (!isConnected())
				return null;
		}
		try {
			Statement stmt = this.connection.createStatement();
			Logger.sql("ReturnQuery "+URL+" :");
			Logger.sql("\t"+query);
			ResultSet rs = stmt.executeQuery(query);
			return rs;
		} catch (SQLException e) {
			Close();
			Connect();
			if (!isConnected())
				return null;
		}
		
		try {
			Statement stmt = this.connection.createStatement();
			Logger.sql("ReturnQuery "+URL+" :");
			Logger.sql("\t"+query);
			ResultSet rs = stmt.executeQuery(query);
			return rs;
		} catch (SQLException e) {
			Logger.sql("Excption during MySQLDatabase.ReturnQuery('"+query+"') :");
			Logger.sql(e.toString());
			return null;
		}
	}

	/* Sendet ein Query und erwartet eine R�ckgabe in Form eines ResultSet */

	public boolean RunQuery(String query) {
		if (!connected) {
			Connect();
			if (!isConnected())
				return false;
		}
		try {
			Statement stmt = this.connection.createStatement();
			Logger.sql("ExecuteQuery "+URL+" :");
			Logger.sql("\t"+query);
			stmt.execute(query);
			return true;
		} catch (Exception e) {
			Close();
			Connect();
			if (!isConnected())
				return false;
		}
		try {
			Statement stmt = this.connection.createStatement();
			Logger.sql("ExecuteQuery "+URL+" :");
			Logger.sql("\t"+query);
			stmt.execute(query);
			return true;
		} catch (Exception e) {
			Logger.sql("Excption during MySQLDatabase.RunQuery('"+query+"') :");
			Logger.sql(e.toString());
			return false;
		}
	}
	/* F�hrt das query aus, erwartet aber keine R�ckantwort des Servers. */

}