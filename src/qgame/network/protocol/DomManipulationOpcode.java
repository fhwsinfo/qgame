package qgame.network.protocol;

public enum DomManipulationOpcode {

	Append("append"),
	Prepend("prepend"),
	Replace("replace"),
	Copy("copy"),
	Move("move"),
	Remove("remove");
	
	private String code;
	
	DomManipulationOpcode (String code) {
		this.code = code;
	}
	
	@Override
	public String toString() {
		return code;
	}
	
}
