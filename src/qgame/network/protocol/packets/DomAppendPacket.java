package qgame.network.protocol.packets;

import qgame.network.protocol.DomManipulationOpcode;

public class DomAppendPacket extends SimpleDomManipulationPacket {

	public DomAppendPacket(String source, String target) {
		super(DomManipulationOpcode.Append, source, target);
	}

}
