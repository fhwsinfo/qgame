package qgame.network.protocol.packets;

import qgame.network.protocol.DomManipulationOpcode;

public class DomReplacePacket extends SimpleDomManipulationPacket {

	public DomReplacePacket(String source, String target) {
		super(DomManipulationOpcode.Replace, source, target);
	}

}
