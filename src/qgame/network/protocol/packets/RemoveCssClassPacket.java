package qgame.network.protocol.packets;

import qgame.network.protocol.Opcode;
import qgame.network.protocol.Packet;

public class RemoveCssClassPacket implements Packet {

	private String target;
	private String cssClass;
	
	public RemoveCssClassPacket(String target, String cssClass) {
		this.target = target;
		this.cssClass = cssClass;
	}
	
	@Override
	public String generate() {
		String ret = "{";
		ret += "\"opcode\":\""+Opcode.RemoveClass.toString()+"\",";
		ret += "\"target\":\""+target+"\",";
		ret += "\"value\":\""+cssClass+"\"}";
		return ret;
	}
	
}
