package qgame.network.protocol.packets;

import qgame.network.protocol.Opcode;
import qgame.network.protocol.Packet;

public class AddCssClassPacket implements Packet {

	private String target;
	private String cssClass;
	
	public AddCssClassPacket(String target, String cssClass) {
		this.target = target;
		this.cssClass = cssClass;
	}
	
	@Override
	public String generate() {
		String ret = "{";
		ret += "\"opcode\":\""+Opcode.AddClass.toString()+"\",";
		ret += "\"target\":\""+target+"\",";
		ret += "\"value\":\""+cssClass+"\"}";
		return ret;
	}

}
