package qgame.network.protocol.packets;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import qgame.network.protocol.Packet;
import qgame.network.protocol.TemplateOpcode;
import qgame.network.protocol.TemplateType;

public class TemplatePacket implements Packet {
	
	public static final TemplatePacket gameLayoutTemplatePacket = new TemplatePacket(TemplateType.Game_Layout);
	public static final TemplatePacket loginTemplatePacket = new TemplatePacket(TemplateType.Login);
	
	private Map<String,String> keys = new HashMap<String,String>();
	private TemplateOpcode opcode;
	private String target;
	private String id;
	
	public TemplatePacket(TemplateOpcode opcode, String id, String target) {
		this.opcode = opcode;
		this.id = id;
		this.target = target;
	}
	
	public TemplatePacket(TemplateOpcode opcode, TemplateType tType, String target) {
		this(opcode, tType.id, target);
	}
	
	public TemplatePacket(TemplateType tType) {
		this(tType.defaultOpcode, tType.id, tType.defaultTargetId);
	}
	
	public void put(String key, String value) {
		keys.put(key, value);
	}
	
	public String generate() {
		String out = "{\"opcode\":\""+opcode.toString()+"\",\"template\":\""+id+"\",\"target\":\""+target+"\",\"keys\":{";
		boolean comma = false;
		for (Entry<String, String> entry : keys.entrySet()) {
			if (comma)
				out+=",";
			else
				comma = true;
			out += "\""+entry.getKey()+"\":\""+entry.getValue()+"\"";
		}
		out += "}}";
		return out;
	}
	
}
