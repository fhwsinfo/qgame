package qgame.network.protocol.packets;

import qgame.network.protocol.Opcode;
import qgame.network.protocol.Packet;

public class SetCookiePacket implements Packet {

	private String name, value;
	
	public SetCookiePacket(String cookieName, String value) {
		this.name = cookieName;
		this.value = value;
	}
	
	@Override
	public String generate() {
		String ret = "{";
		ret += "\"opcode\":\""+Opcode.SetCookie.toString()+"\",";
		ret += "\"name\":\""+name+"\",";
		ret += "\"value\":\""+value+"\"}";
		return ret;
	}

}
