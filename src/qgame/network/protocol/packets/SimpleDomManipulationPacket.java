package qgame.network.protocol.packets;

import qgame.network.protocol.DomManipulationOpcode;
import qgame.network.protocol.Packet;

public class SimpleDomManipulationPacket implements Packet {

	private DomManipulationOpcode opcode;
	private String source;
	private String target;
	
	public SimpleDomManipulationPacket(DomManipulationOpcode opcode, String source, String target) {
		this.opcode = opcode;
		this.source = source;
		this.target = target;
	}
	
	@Override
	public String generate() {
		String ret = "{";
		ret += "\"opcode\":\""+opcode.toString()+"\",";
		ret += "\"target\":\""+target+"\"";
		if (opcode == DomManipulationOpcode.Remove)
			return ret+"}";
		ret += ",\"source\":\""+source+"\"}";
		return ret;
	}

}
