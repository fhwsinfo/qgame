package qgame.network.protocol.packets.menu;

import qgame.game.menu.MenuSectionType;
import qgame.network.protocol.TemplateType;
import qgame.network.protocol.packets.TemplatePacket;

public class MenuSectionPacket extends TemplatePacket {
	
	
	public MenuSectionPacket(MenuSectionType sectionType) {
		super(TemplateType.Menu_Section);
		put("$name$", sectionType.name);
		put("$css$", sectionType.css);
		put("$id$", ""+sectionType.id);
	}

}
