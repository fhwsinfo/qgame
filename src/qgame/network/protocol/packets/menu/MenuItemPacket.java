package qgame.network.protocol.packets.menu;

import qgame.game.menu.MenuItemType;
import qgame.network.protocol.TemplateOpcode;
import qgame.network.protocol.TemplateType;
import qgame.network.protocol.packets.TemplatePacket;

public class MenuItemPacket extends TemplatePacket {

	public MenuItemPacket(MenuItemType menuType) {
		super(TemplateOpcode.Append, TemplateType.Menu_Item, "caption_"+menuType.parent.id);
		put("$id$",""+menuType.id);
		put("$name$", menuType.name);
		put("$script$", menuType.script);
	}

}
