package qgame.network.protocol.packets;

import qgame.network.protocol.DomManipulationOpcode;

public class DomCopyPacket extends SimpleDomManipulationPacket{

	public DomCopyPacket(String source, String target) {
		super(DomManipulationOpcode.Copy, source, target);
	}
	
}
