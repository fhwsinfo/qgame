package qgame.network.protocol;


public enum TemplateType {
	
	Login("template_login", TemplateOpcode.Replace, "main"),
	Game_Layout("template_game_layout", TemplateOpcode.Replace, "main"),
	Chat_Message("template_chat_message", TemplateOpcode.Prepend, "chat_messages"),
	Menu_Section("template_menu_section", TemplateOpcode.Append, "menu_main"),
	Menu_Item("template_menu_item", TemplateOpcode.Append, "caption_$id$"),
	Console_Message("template_console_message", TemplateOpcode.Append, "console_messages"),
	Console_Checkbox("template_console_logtype_selector", TemplateOpcode.Prepend, "consoleCheckboxList");
	
	public final String id;
	public final TemplateOpcode defaultOpcode;
	public final String defaultTargetId;
	
	TemplateType(String id, TemplateOpcode defaultMethod, String defaultTargetId) {
		this.id = id;
		this.defaultOpcode = defaultMethod;
		this.defaultTargetId = defaultTargetId;
	}
	
}
