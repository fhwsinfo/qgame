package qgame.network.protocol;

public enum TemplateOpcode {
	
	Append("append"),
	Prepend("prepend"),
	Replace("replace");
	
	private String code;
	
	TemplateOpcode (String code) {
		this.code = code;
	}
	
	@Override
	public String toString() {
		return code;
	}
	
}
