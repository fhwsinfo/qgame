package qgame.network.protocol;

import java.util.HashMap;
import java.util.Map;

public enum Opcode {
	
	// Client -> Server Opcodes
	Init("init"),
	FormSubmit("formsubmit"),
	MenuSwitch("switchmenu"),
	
	// Server -> Client Opcodes
	
	// Dom-Object manipulation
	Append("append"),
	Prepend("prepend"),
	Replace("replace"),
	Copy("copy"),
	Move("move"),
	Remove("remove"),
	
	// CSS manipulation
	AddClass("addclass"),
	RemoveClass("removeclass"),
	
	// other
	SetCookie("setcookie");
	
	private static Map<String, Opcode> opcodes;
	
	static {
		opcodes = new HashMap<String, Opcode>();
		for (Opcode opcode : Opcode.values()) {
			opcodes.put(opcode.toString(), opcode);
		}
	}
	
	public static Opcode getOpcode(String code) {
		return opcodes.get(code);
	}
	
	private String code;
	
	Opcode (String code) {
		this.code = code;
	}
	
	@Override
	public String toString() {
		return code;
	}
}
