package qgame.network.protocol;

public interface Packet {
	
	public String generate();
	
}
