package qgame;

import java.sql.ResultSet;

import javax.swing.SwingUtilities;

import qgame.config.Config;
import qgame.game.chat.ChatBuffer;
import qgame.game.universe.Universe;
import qgame.log.Logger;
import qgame.network.netty.WebSocketServer;
import qgame.network.sql.DatabaseManager;
import qgame.threading.SocketThreadFactory;

import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintStream;

import qgame.console.QGameConsole;
import qgame.console.SystemErrHandler;
import qgame.console.SystemOutHandler;
import qgame.game.permissions.PermissionGroup;
import qgame.network.netty.WebSocketHandler;
import qgame.session.PlayerSession;
import qgame.threading.SocketWorker;
import qgame.network.sql.DatabaseType;
import qgame.network.sql.Request;

/**
 * 
 * Main class of the Game. 
 * Starts all the necessary Threads including Console, Database and SocketServer
 * 
 * @author Kagamul
 *
 */
public class QGame {

	/**
	 * Port the WebSocketServer will run on.
	 */
	
	private static int port;
	
	
	public static Config mainConfig;
	public static WebSocketServer webSocketServer;
	public static boolean consoleEnabled = true;
	public static boolean shutdown = false;
	
	private static SocketThreadFactory factory;
	private static QGameConsole console;
	
	private static String config_path   = "qgame.cfg";
	private static String keystore_path = "keystore.jks";
	private static String pid_path      = "qgame.pid";
	
	public static void main(String[] args) throws Exception {

		for (String str : args) {
			if (str.trim().toLowerCase().equals("-nogui"))
				consoleEnabled = false;
			else if (str.trim().toLowerCase().indexOf("-config=") > -1)
				config_path    = str.trim().toLowerCase().substring(8);
			else if (str.trim().toLowerCase().indexOf("-keystore=") > -1)
				keystore_path  = str.trim().toLowerCase().substring(10);
			else if (str.trim().toLowerCase().indexOf("-pid=") > -1)
				pid_path       = str.trim().toLowerCase().substring(5);
		}
		
		
		String pid = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
		pid = pid.split("@")[0];
		
		try {
			File file = new File(pid_path);
			BufferedWriter output = new BufferedWriter(new FileWriter(file));
			output.write(pid);
			output.close();
		} catch ( Exception e ) {}
		
		Runtime.getRuntime().addShutdownHook(new Thread(() -> shutdown()));
		
		
		
		if (consoleEnabled) {
			// Open Console, SwingUtilities has to be used for proper handling of the JFrame
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					console = new QGameConsole();
				}
			});
		
			// Wait until console is opened
			// otherwise System.out/err might not get displayed
			while (!QGameConsole.consoleOpen) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {}
			}

		}

		//Redirect Systm.out/err
		PrintStream printStream = new PrintStream(new SystemOutHandler());
		System.setOut(printStream);
		printStream = new PrintStream(new SystemErrHandler());
		System.setErr(printStream);
		
		//Read config file
		mainConfig = new Config(config_path);
		mainConfig.read();
		
		port = mainConfig.get("websocket_server_port", 8080, 1, 49151);
		
		// Initialize DatabaseManager
		DatabaseManager.initialize();
		
		//TODO: Listener f�r Sigterm 15 -> http://stackoverflow.com/questions/2541597/how-to-gracefully-handle-the-sigkill-signal-in-java
		
		// Write config to disk
		mainConfig.write();
		
		// Start the MySQL DatabaseManager
		new Thread(() -> DatabaseManager.workLoop()).start();
		
		// Wait until DbManager is running
		while (!DatabaseManager.isReady()) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {}
		}
		
		factory = new SocketThreadFactory("factory");
		for (int x = 0; x < 16; x++) {
			factory.createThread(new SocketWorker(), "SocketWorker_"+(x+1));
		}
		
		initialize();
		
		// Start the WebSocketServer, if the server can't be started (most likely because of an occupied port)
		// an Exception will be thrown
		try {
			webSocketServer = new WebSocketServer(port);
			webSocketServer.run(); // The main Thread will remain in here until the server shuts down
		} catch (Exception e) {
			Logger.error("Failed to start the server! Port already occupied?");
			Logger.error(e.toString());
		}
		

	}
	
	public static void initialize() {
		String query = "SELECT * FROM get_chat_messages ORDER BY time ASC;";
		Request req = new Request(null, query, DatabaseType.Log, (ResultSet result) -> ChatBuffer.init(result));
		DatabaseManager.sheduleRequest(req);
		
		query = "SELECT * FROM groups;";
		req = new Request(null, query, DatabaseType.Account, (ResultSet result) -> PermissionGroup.addPermissionGroups(result));
		DatabaseManager.sheduleRequest(req);
		

		// Initialize Universe
		if (Universe.getInstance() != null)
			Logger.info("Universe initialized!");
		else
			Logger.error("Failed to initi Universe!");
	}
	
	public static void shutdown() {
		Logger.info("shutting down...");
		shutdown = true;
		factory.shutdown();
		PlayerSession.destroyAllSessions();
		WebSocketHandler.destroyAllSockets();
		DatabaseManager.shutdown();
		QGame.webSocketServer.close();
		WebSocketHandler.shutdown();
		if (consoleEnabled)
			console.dispatchEvent(new WindowEvent(console, WindowEvent.WINDOW_CLOSING));
	}
	
	public static SocketThreadFactory getSocketThreadFactory() {
		return factory;
	}

	public static String getKeystore_path() {
		return keystore_path;
	}

	
}
