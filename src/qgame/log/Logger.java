package qgame.log;

import java.io.Console;

import qgame.QGame;
import qgame.console.ConsoleOutputBuffer;
import qgame.console.QGameConsole;
import qgame.tools.Time;

public class Logger { //TODO log-file implementation
	
	private static final Console console = System.console();
	
	private static Long timeDiffLog = 0L;

	public static void log(String line, LogLevel logLevel) {
		String time = Time.currentTimeString();
		if (QGame.consoleEnabled)
			QGameConsole.println(time +" : "+line);
		ConsoleOutputBuffer.addLine(time, line, logLevel.logType);
		if (console != null)
			console.printf(line);
	}
	
	public static void error(String line) {
		log(line, LogLevel.Error);
	}
	
	public static void debug(String line) {
		log(line, LogLevel.Debug);
	}
	
	public static void info(String line) {
		log(line, LogLevel.Info);
	}
	
	public static void sql(String line) {
		log(line, LogLevel.Sql);
	}
	
	public static void resetTimeDiff() {
		synchronized (timeDiffLog) {
			timeDiffLog = System.currentTimeMillis();
		}
	}
	
	public static void timeDiff(String msg) {
		long time;
		synchronized (timeDiffLog) {
			time = System.currentTimeMillis() - timeDiffLog;
			timeDiffLog = System.currentTimeMillis();
		}
		debug("TimeDiffLog ("+msg+"): "+time+"ms");
	}
	
}
