package qgame.log;

public enum LogLevel {

	Debug	("log_type_debug"),
	Info	("log_type_info"),
	Error	("log_type_error"),
	Sql		("log_type_sql");
	
	public final String logType;
	
	private LogLevel(String logType) {
		this.logType = logType;
	}
	
}
