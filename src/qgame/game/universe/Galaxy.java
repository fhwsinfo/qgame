package qgame.game.universe;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import qgame.log.Logger;
import qgame.network.sql.DatabaseManager;
import qgame.network.sql.DatabaseType;
import qgame.network.sql.Request;

public class Galaxy {
	Map<Integer, Solarsystem> _solarsystems= new HashMap<Integer, Solarsystem>();
	
	int m_ID;
	int m_solarcount;
	String m_name;
	
	
	Galaxy(int galaxy_id,int solar_count,String name) {
		m_ID = galaxy_id;
		m_solarcount = solar_count;
		m_name = name;
		String query = "SELECT * FROM game.galaxy_solarsystem WHERE parent_galaxy_uid = "+galaxy_id+";";
		Request req = new Request(null, query, DatabaseType.Game, (ResultSet result) -> initSolarsystems(result));
		DatabaseManager.sheduleRequest(req);
		
		cacheToDb(); // Galaxien IMMER cachen!
	}
	
	private void initSolarsystems(ResultSet result) {
		int solar_id     = 0;
		int planet_count = 0;
		int position     = 0;
		
		try {
			while (result.next()) {
				solar_id     = result.getInt("uid"); 
				planet_count = result.getInt("planet_count");
				position     = result.getInt("position");
				
				if (solar_id > Universe.last_solar_id)
					Universe.last_solar_id = solar_id;
				
				
				generateSolar(solar_id,position,planet_count);
			}
		} catch (SQLException e) {
			Logger.sql("SQLException in Galaxy.initSolarsystems :");
			Logger.sql(e.toString());
		}
		/*
		for (int i = 1; i <= m_solarcount; i++) 
			if (_solarsystems.get(i) == null) {
				generateSolar(i);
			}
		 */
		getSolarsystem(6);
	}

	public boolean generateSolar(int solar_pos) {
		return generateSolar(solar_pos,0);
	}
	public boolean generateSolar(int solar_pos,int solar_id) {
		int rand = (int) (3+Math.abs(new Random().nextGaussian()*2)+Math.abs(new Random().nextGaussian()*2));
		return generateSolar(solar_pos,solar_id,rand);
	}
	private boolean generateSolar(int solar_pos,int solar_id,int planetcount) {
		Solarsystem _new = _solarsystems.get(solar_pos);
		if (_new != null)
			return false;
		
		if (solar_id == 0)
			solar_id = ++Universe.last_solar_id;
		
		Logger.debug(String.format("**  Generate Solar %d at [%d:%d] with %d Planets!.\n",solar_id,m_ID,solar_pos,planetcount));
		
		_new = new Solarsystem(this,m_ID,solar_id,solar_pos,planetcount);
		_solarsystems.put(solar_pos,_new);
		Universe.getInstance()._solar_id.put(solar_pos, _new);
		return true;
	}

	
	public Solarsystem getSolarsystem(int solar_pos) {
		Solarsystem out = _solarsystems.get(solar_pos);
		if (out == null) {
			generateSolar(solar_pos);
			out = _solarsystems.get(solar_pos);
		}
		return out;
	}
	
	public void cacheToDb() {
		String query = "INSERT INTO game.galaxy (uid,solarsystem_count,name) VALUES ("+m_ID+","+m_solarcount+",\""+m_name+"\")";
		query = query+ " ON DUPLICATE KEY UPDATE solarsystem_count = "+m_solarcount+", name = \""+m_name+"\";";
		Request req = new Request(null, query, DatabaseType.Account, (Runnable) null);
		DatabaseManager.sheduleRequest(req);
		Logger.debug(String.format("* Cached Galaxy %d\n",m_ID));
	}
	
	public Solarsystem getRandomSolarsystem() {
		return getSolarsystem((int)(Math.random() * m_solarcount)+1);
	}
	
}
