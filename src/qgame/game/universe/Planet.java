package qgame.game.universe;

import java.sql.ResultSet;
import java.sql.SQLException;

import qgame.log.Logger;
import qgame.network.sql.DatabaseManager;
import qgame.network.sql.DatabaseType;
import qgame.network.sql.Request;
import qgame.tools.parser.Parser;

public class Planet {
	int owner_id;

	int uid;
	int position;
	
	Solarsystem parent_solar;
	Galaxy 		parent_galaxy;
	String name;
	
	public enum Ressource {
		RESSOURCE_METAL(0,"2n*n+3"),
		RESSOURCE_CRYSTAL(1,"5n+9"),
		RESSORUCE_GAS(2,"9n"),
		RESSORUCE_REFINED_GAS(3,"4n");
		
		private int id;
		private String formula;
		
		private Ressource(int id,String n) {
		  this.id = id;
		  this.formula = n;
		 }
		 
	};
	double[] ressource = {5000,1000,0,0};
	
	long last_sync ;
	
	
	Planet(Galaxy pg, Solarsystem ps,int planet_uid, int planet_pos, String name) {
		parent_galaxy = pg;
		parent_solar = ps;
		this.uid = planet_uid;
		this.position = planet_pos;
		this.name = name;
		

		String query = "SELECT * FROM game.galaxy_planet WHERE uid = "+planet_uid+";";
		Request req = new Request(null, query, DatabaseType.Game, (ResultSet result) -> initPlanet(result));
		DatabaseManager.sheduleRequest(req);
		
		last_sync = System.currentTimeMillis();
	}
	private void initPlanet(ResultSet result)  {
		try {
			if(result.next()) {

				for (Ressource ress : Ressource.values()) {
					int i = ress.id;
					ressource[i] = result.getInt("amount_resource_"+(i+1));
				}
				this.last_sync = result.getLong("last_resource_update");
			}
		} catch (SQLException e) {
			Logger.sql("SQLException in Planet.initPlanet :");
			Logger.sql(e.toString());
		}
		last_sync =  System.currentTimeMillis(); 
		recalcRessources();
	}
	
	public void recalcRessources() {
		long now = System.currentTimeMillis(); 
		if (now <= last_sync)
			return;
		
		int seconds = (int) ((last_sync-now)/1000);
		
		for (Ressource ress : Ressource.values()) {
		  Parser p = new Parser();
		  p.addVariable("n", (double) seconds);
		  ressource[ress.id]+=p.parse(ress.formula);
		}
		last_sync =  System.currentTimeMillis(); 
		
	}

	public void cacheToDb() {
		String query = "REPLACE INTO game.galaxy_planet (uid,position,parent_solar,owner_id,name,amount_resource_1,amount_resource_2,amount_resource_3,amount_resource_4,last_resource_update) VALUES (%d,%d,%d,%d,'%s',%d,%d,%d,%d,%d);";
		query = String.format(query, uid,position,parent_solar.m_ID,owner_id,name,(int)ressource[0],(int)ressource[1],(int)ressource[2],(int)ressource[3],last_sync);
		Request req = new Request(null, query, DatabaseType.Game, (Runnable) null);
		
		DatabaseManager.sheduleRequest(req);
		Logger.debug(String.format("* Cached Planet %d [%d:%d:%d] \n",uid,parent_galaxy.m_ID,parent_solar.m_position,position));
	}
}
