package qgame.game.universe;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import qgame.log.Logger;
import qgame.network.sql.DatabaseManager;
import qgame.network.sql.DatabaseType;
import qgame.network.sql.Request;

/*
 * �berlegung:
 * 	Das Universum w�chst mit der Anzahl der kolonisierten Planeten.
 * 
 * Punkt 1:
 * 		Es sind immer Doppelt so viele Planeten verf�gbar wie kolonisiert.
 * Punkt 2:
 *      Ein Sonnensystem fast zwischen 3 und 10 (?) Planeten.
 * Punkt 3:
 *  	Eine Galaxie umfasst bis zu 100 Sonnensysteme
 *  
 *  Wird ein neuer Planet Kolonisiert, muss der Universum-Generator pr�fen, ob gen�gend Planeten verbleiben.
 *  Das gleiche beim Hochfahren des Servers!
 * 
 * 
 */
public class Universe {

	
	private static Universe instance;
	
	public static Universe getInstance() {
		if (Universe.instance == null) {
			Universe.instance = new Universe();
		}
		return Universe.instance;
	}
	
	Map<Integer, Galaxy> _galaxies= new HashMap<Integer, Galaxy>();
	
	
	Map<Integer, Solarsystem> _solar_id  = new HashMap<Integer, Solarsystem>();
	Map<Integer, Planet> _planet_id = new HashMap<Integer, Planet>();
	
	public static int last_solar_id = 0;
	public static int last_planet_id = 0;
	
	// INIT
	Universe() { 
		Logger.info("Initializing Universe");
		String query = "SELECT * FROM galaxy;";
		Request req = new Request(null, query, DatabaseType.Game, (ResultSet result) -> getInstance().initGalaxies(result));
		DatabaseManager.sheduleRequest(req);
	}
	
	private void initGalaxies(ResultSet result) {
		int galaxy_id   = 0;
		int solar_count = 0;
		String galaxy_name = "";
		
		int count = 0;
		Logger.info("Initializing Galaxies");
		try {
			while (result.next()) {
				galaxy_id =   result.getInt("uid"); 
				solar_count = result.getInt("solarsystem_count");
				galaxy_name = result.getString("name");
				
				generateGalaxy(galaxy_id,solar_count,galaxy_name);
				
				count++;
			}
		} catch (SQLException e) {
			Logger.sql("SQLException in Unierse.initGalaxies :");
			Logger.sql(e.toString());
		}
		
		if (count == 0) {
			// Noch keine Galaxien in der Datenbank :O
			// TODO: Erstelle Routine zum erstellen von Galaxien! 
		}
		
	}

	private boolean generateGalaxy(int galaxy_pos) {
		return generateGalaxy(galaxy_pos,"Galaxie");
	}
	private boolean generateGalaxy(int galaxy_pos,String name) {
		int rand = (int) (20+Math.abs(new Random().nextGaussian()*12)+Math.abs(new Random().nextGaussian()*12));
		return generateGalaxy(galaxy_pos,rand,name);
	}
	private boolean generateGalaxy(int galaxy_pos,int solarcount,String name) {
		Galaxy _new = _galaxies.get(galaxy_pos);
		if (_new != null)
			return false;
		
		Logger.debug(String.format("* Generate Galaxy at [%d] with %d Solarsystems!.\n",galaxy_pos,solarcount));
		
		_new = new Galaxy(galaxy_pos,solarcount,name);
		_galaxies.put(galaxy_pos,_new);
		return true;
	}

	public Galaxy getGalaxy(int _galaxy_pos) {
		int galaxy_pos = Math.max(Math.min(_galaxy_pos,100),1);
		Galaxy out = _galaxies.get(galaxy_pos);
		if (out == null) {
			generateGalaxy(galaxy_pos);
			out = _galaxies.get(galaxy_pos);
		}
		return out;
	}
	public Solarsystem getSolarsystem(int galaxy_pos,int solar_pos) {
		return getGalaxy(galaxy_pos).getSolarsystem(solar_pos);
	}

	public Solarsystem getSolarsystem(int solar_id) { // CAN BE NULL
		return _solar_id.get(solar_id);
	}
	
	public Planet getPlanet(int galaxy_pos,int solar_pos,int planet_pos) {
		return getGalaxy(galaxy_pos).getSolarsystem(solar_pos).getPlanet(planet_pos);
	}
	
	
	public Planet getPlanet(int planet_id) { // CAN BE NULL
		return _planet_id.get(planet_id);
	}
	// TODO: Prozedual gestalten!!
	public Planet GetFreePlanet() {
		Galaxy gal = getGalaxy(1);
		Solarsystem sol;
		do {
			sol = gal.getRandomSolarsystem();
		} while (!sol.hasFreePlanets()); 
		return sol.getRandomFreePlanet();
	}
	
}
