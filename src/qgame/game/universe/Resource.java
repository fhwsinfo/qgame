package qgame.game.universe;

// TODO: Ressourcen auslagern in diese Klasse

public class Resource {
	int amount = 0;

	private String formula = "12";
	
	Resource() {
		
	}

	
	/**
	 * @return the formula
	 */
	public String getFormula() {
		return formula;
	}

	/**
	 * @param formula the formula to set
	 */
	public void setFormula(String formula) {
		this.formula = formula;
	}
	
}
