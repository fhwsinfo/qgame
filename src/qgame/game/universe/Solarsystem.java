package qgame.game.universe;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import qgame.log.Logger;
import qgame.network.sql.DatabaseManager;
import qgame.network.sql.DatabaseType;
import qgame.network.sql.Request;

public class Solarsystem {

	Map<Integer, Planet> _planets= new HashMap<Integer, Planet>();
	
	int m_galaxy, m_ID,m_position;
	int m_planet_count,m_planet_used;
	
	Galaxy parent_galaxy;
	
	public Solarsystem(Galaxy parent,int galaxy_id, int solar_id, int position, int planet_count) {
		parent_galaxy = parent;
		m_galaxy = galaxy_id;
		m_ID = solar_id;
		m_position = position;
		m_planet_count = planet_count;
		m_planet_used = 0;
		
		cacheToDb() ;
		
		String query = "SELECT * FROM game.galaxy_planet WHERE parent_solar = "+solar_id+";";
		Request req = new Request(null, query, DatabaseType.Game, (ResultSet result) -> initPlanets(result));
		DatabaseManager.sheduleRequest(req);
	}
	
	private void initPlanets(ResultSet result) {
		int planet_uid     = 0;
		int planet_pos     = 0;
		String planet_name = "";
		
		try {
			while (result.next()) {
				planet_uid  =   result.getInt("uid"); 
				planet_pos  =   result.getInt("position");
				planet_name =   result.getString("name");
				
				planet_name = (planet_name == "") ? "Planet" : planet_name;

				
				if (planet_uid > Universe.last_planet_id)
					Universe.last_planet_id = planet_uid;
				
				
				generatePlanet(planet_pos,planet_name,planet_uid);
			}
		} catch (SQLException e) {
			Logger.sql("SQLException in Solarsystem.initPlanets :");
			Logger.sql(e.toString());
		}
		if (m_planet_used < m_planet_count) {
			Logger.debug(String.format("**  Solar %d [%d:%d] is not complete (%d of %d)!\n",m_ID,m_galaxy,m_position,m_planet_used,m_planet_count));
		}
	}

	public boolean generatePlanet(int planet_pos) {
		return generatePlanet(planet_pos,"Planet", 0);
	}
	public boolean generatePlanet(int planet_pos,String name) {
		return generatePlanet(planet_pos,name, 0);
	}
	public boolean generatePlanet(int planet_pos,String name,int planet_id) {
		Planet _new = _planets.get(planet_pos);
		if (_new != null)
			return false;
		
		if (planet_id == 0)
			planet_id = ++Universe.last_planet_id;
		
		Logger.debug(String.format("*** Generate Planet %d at [%d:%d:%d]!\n",planet_id,m_galaxy, m_ID,planet_pos));
		
		_new = new Planet(parent_galaxy,this,planet_id,planet_pos,name);
		_planets.put(planet_pos,_new);
		Universe.getInstance()._planet_id.put(planet_id, _new);
		return true;
	}
	
	public Planet getPlanet(int planet_pos) {
		Planet out = _planets.get(planet_pos);
		if (out == null) {
			generatePlanet(planet_pos);
			out = _planets.get(planet_pos);
		}
		return out;
	}
	public void cacheToDb() {
		String query = "INSERT INTO game.galaxy_solarsystem (uid,position,parent_galaxy_uid,planet_count) VALUES ("+m_ID+","+m_position+","+m_galaxy+","+m_planet_count+")";
		query = query+ " ON DUPLICATE KEY UPDATE position = "+m_position+", parent_galaxy_uid = "+m_galaxy+", planet_count = "+m_planet_count+";";
		Request req = new Request(null, query, DatabaseType.Account, (Runnable) null);
		DatabaseManager.sheduleRequest(req);
		Logger.debug(String.format("* Cached Solarsystem %d [%d:%d] \n",m_ID,m_galaxy,m_position));
	}
	
	
	public boolean hasFreePlanets() {
		int free = m_planet_count;
		for (int n = 1; n <= m_planet_count; n++) {
			Planet plan = _planets.get(n);
			if (plan != null && plan.owner_id != 0) {
				free --;
			}
		}
		return free > 0;
	}
	
	public Planet getRandomFreePlanet() {
		int i = 0;
		Planet plan;
		do {
			i = (int)(Math.random()*m_planet_count)+1;
			plan = _planets.get(i);
		} while (plan == null || plan.owner_id != 0);
		return getPlanet(i);
	}
	
}
