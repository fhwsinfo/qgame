package qgame.game.permissions;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import qgame.log.Logger;
import qgame.network.sql.DatabaseManager;
import qgame.network.sql.DatabaseType;
import qgame.network.sql.Request;


public class PermissionGroup {

	private static Map<Integer, PermissionGroup> permissionGroups = new HashMap<Integer, PermissionGroup>();
	private static Map<String, PermissionGroup> permissionGroups2 = new HashMap<String, PermissionGroup>();
	
	public static PermissionGroup get(int id) {
		return permissionGroups.get(id);
	}
	
	public static PermissionGroup get(String name) {
		return permissionGroups2.get(name);
	}
	
	public static void addPermissionGroups(ResultSet result) {
		int id;
		int priority;
		String name;
		try {
			while (result.next()) {
				id = result.getInt("gid");
				name = result.getString("name");
				priority = result.getInt("priority");
				new PermissionGroup(id, name.toLowerCase(), priority);
			}
		} catch (SQLException e) {
			Logger.sql("SQLException in PermissionGroup.addPermissionGroups :");
			Logger.sql(e.toString());
		}
	}
	
	private final int id;
	private final int priority;
	protected final Map<String, String> permissions = new HashMap<String, String>();
	
	
	public PermissionGroup(int id, String name, Integer priority) {
		this.id = id;
		if (priority != null)
			this.priority = priority;
		else
			this.priority = 0;
		String query;
		Request req;
		if (name != null) {
			permissionGroups.put(id, this);
			permissionGroups2.put(name, this);
			query = "SELECT * FROM group_permission WHERE `gid` = "+id+";";
			req = new Request(null, query, DatabaseType.Account, (ResultSet result) -> setPermissions(result));
			DatabaseManager.sheduleRequest(req);
		} else {
			query = "SELECT * FROM permissions WHERE `account_id` = "+id+";";
			req = new Request(priority, query, DatabaseType.Account, (ResultSet result) -> setPermissions(result));
			DatabaseManager.sheduleRequest(req);
		}
	}
	
	protected void setPermissions(ResultSet result) {
		String perm, value;
		try {
			while (result.next()) {
				perm = result.getString("permission");
				value = result.getString("permission_value");
				setPermission(perm.toUpperCase(), value.toLowerCase(), true);
			}
		} catch (SQLException e) {
			Logger.sql("SQLExeption in PermissionGroup.setPermissions :");
			Logger.sql(e.toString());
		}
	}
	
	public void setPermission(String perm, String value) {
		setPermission(perm, value, false);
	}
	
	public void setPermission(String perm, String value, boolean silent) {
		perm = perm.toUpperCase();
		value = value.toLowerCase();
		permissions.put(perm, value);
		if (silent)
			return;
		String test = permissions.get(perm);
		if (test.equals(value))
			return;
		String query = "REPLACE INTO group_permission (gid, permission, permission_value) VALUES ("+id+", '"+perm+"', '"+value+"');";
		Request req = new Request(null, query, DatabaseType.Account, (Runnable) null);
		DatabaseManager.sheduleRequest(req);
	}
	
	public String getPermission(String perm) {
		return permissions.get(perm);
	}
	
	public int getId() {
		return id;
	}
	
	public int getPriority() {
		return priority;
	}
	
}
