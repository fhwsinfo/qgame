package qgame.game.menu;

import java.util.HashMap;
import java.util.Map;

public enum MenuSectionType {
	
	Common(0, "Allgemein", "common"),
	Social(1, "Sozial", "social"),
	Other(2, "Sonstiges", "other"),
	Admin(3, "Administration", "admin");
	
	private static Map<Integer, MenuSectionType> captionTypes;
	
	static {
		captionTypes = new HashMap<Integer, MenuSectionType>();
		for (MenuSectionType type : MenuSectionType.values()) {
			captionTypes.put(type.id, type);
		}
	}
	
	public static MenuSectionType getCaptionType(int id) {
		return captionTypes.get(id);
	}
	
	public final int id;
	public final String name;
	public final String css;
	
	MenuSectionType(int id, String name, String css) {
		this.id = id;
		this.name = name;
		this.css = css;
	}
	
}
