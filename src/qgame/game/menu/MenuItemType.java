package qgame.game.menu;

import java.util.HashMap;
import java.util.Map;

public enum MenuItemType {
	
	Overview(0, "&Uuml;bersicht", "switchMenu(0);", MenuSectionType.Common, "overview"),
	Buildings(1, "Geb&auml;ude", "switchMenu(1);", MenuSectionType.Common, "building"),
	Research(2, "Forschung", "switchMenu(2);", MenuSectionType.Common, "research"),
	Defense(3, "Verteidigung", "switchMenu(3);", MenuSectionType.Common, "defense"),
	Shipyard(4, "Schiffswerft", "switchMenu(4);", MenuSectionType.Common, "shipyard"),
	Fleet(5, "Flottenbewegung", "switchMenu(5);", MenuSectionType.Common, "fleet"),
	Techtree(6, "Technologiebaum", "switchMenu(6);", MenuSectionType.Common, "techtree"),
	
	Messages(7, "Nachrichten", "switchMenu(7);", MenuSectionType.Social, "messages"),
	Alliance(8, "Allianz", "switchMenu(8);", MenuSectionType.Social, "alliance"),
	Galaxy(9, "Galaxie&uuml;bersicht", "switchMenu(9);", MenuSectionType.Social, "galaxy"),
	Planets(10, "Planeten&uuml;bersicht", "switchMenu(10);", MenuSectionType.Social, "planets"),
	
	Support(11, "Support", "switchMenu(11);", MenuSectionType.Other, "support"),
	Legal(12, "Impressum", "switchMenu(12);", MenuSectionType.Other, "legal"),
	Logout(13, "Logout", "setCookie('session',0); location.reload();", MenuSectionType.Other, ""),
	
	Debug(14,"Debug", "switchMenu(14);", MenuSectionType.Admin, "admin_debug"),
	Console(15, "Konsole", "switchMenu(15);", MenuSectionType.Admin, "admin_console");
	
	private static Map<Integer, MenuItemType> itemTypes;
	
	static {
		itemTypes = new HashMap<Integer, MenuItemType>();
		for (MenuItemType type : MenuItemType.values()) {
			itemTypes.put(type.id, type);
		}
	}
	
	public static MenuItemType getMenuItemType(int id) {
		return itemTypes.get(id);
	}
	
	public final int id;
	public final String name;
	public final String script;
	public final MenuSectionType parent;
	public final String template;
	
	MenuItemType(int id, String name, String script, MenuSectionType parent, String template) {
		this.id = id;
		this.name = name;
		this.script = script;
		this.parent = parent;
		this.template = template;
	}
	
}