package qgame.game.menu;

import java.util.ArrayList;
import java.util.List;

import qgame.network.protocol.Packet;
import qgame.network.protocol.packets.AddCssClassPacket;
import qgame.network.protocol.packets.RemoveCssClassPacket;
import qgame.network.protocol.packets.SetCookiePacket;
import qgame.network.protocol.packets.menu.MenuItemPacket;
import qgame.network.protocol.packets.menu.MenuSectionPacket;
import qgame.session.PlayerSession;

public class PlayerMenu {

	private static final List<Packet> menuPackets;
	
	static {
		menuPackets = new ArrayList<Packet>();
		
		menuPackets.add(new MenuSectionPacket(MenuSectionType.Common));
		menuPackets.add(new MenuItemPacket(MenuItemType.Overview));
		menuPackets.add(new MenuItemPacket(MenuItemType.Buildings));
		menuPackets.add(new MenuItemPacket(MenuItemType.Research));
		menuPackets.add(new MenuItemPacket(MenuItemType.Defense));
		menuPackets.add(new MenuItemPacket(MenuItemType.Shipyard));
		menuPackets.add(new MenuItemPacket(MenuItemType.Fleet));
		menuPackets.add(new MenuItemPacket(MenuItemType.Techtree));
		
		menuPackets.add(new MenuSectionPacket(MenuSectionType.Social));
		menuPackets.add(new MenuItemPacket(MenuItemType.Messages)); //TODO handle this one outside (for example in PlayerSession) to display current messages
		menuPackets.add(new MenuItemPacket(MenuItemType.Alliance));
		menuPackets.add(new MenuItemPacket(MenuItemType.Galaxy));
		menuPackets.add(new MenuItemPacket(MenuItemType.Planets));
		
		menuPackets.add(new MenuSectionPacket(MenuSectionType.Other));
		menuPackets.add(new MenuItemPacket(MenuItemType.Support));
		menuPackets.add(new MenuItemPacket(MenuItemType.Legal));
		menuPackets.add(new MenuItemPacket(MenuItemType.Logout));
	}
	
	public static void sendMenu(PlayerSession pSess) {
		for (Packet pck : menuPackets) {
			pSess.send(pck);
		}
	}
	
	private PlayerSession pSess;
	private MenuItemType currentMenu = null;
	
	public PlayerMenu(PlayerSession pSess) {
		this.pSess = pSess;
	}
	
	public void switchMenu(MenuItemType target) {
		if (target == null)
			return;
		if (currentMenu == null) {
			if (pSess.hasPermission("MENU:USE_MENUITEM_"+(target.template.toUpperCase()))) {
				currentMenu = target;
				pSess.send(new SetCookiePacket("menuid", target.id+""));
				pSess.onMenuSwitch(currentMenu, target);
				pSess.send(new AddCssClassPacket("menu_item_"+target.id, "selected"));
			}
			return;
		}
		if (currentMenu == target)
			return;
		if (pSess.hasPermission("MENU:USE_MENUITEM_"+(target.template.toUpperCase()))) {
			pSess.onMenuSwitch(currentMenu, target);
			pSess.send(new RemoveCssClassPacket("menu_item_"+currentMenu.id, "selected"));
			pSess.send(new AddCssClassPacket("menu_item_"+target.id, "selected"));
			pSess.send(new SetCookiePacket("menuid", target.id+""));
			currentMenu = target;
		}
	}
	
}
