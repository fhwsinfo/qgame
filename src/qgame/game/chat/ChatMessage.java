package qgame.game.chat;

import java.sql.Timestamp;

import qgame.network.protocol.Packet;
import qgame.network.protocol.TemplateType;
import qgame.network.protocol.packets.TemplatePacket;
import qgame.network.sql.DatabaseManager;
import qgame.network.sql.DatabaseType;
import qgame.network.sql.Request;
import qgame.tools.Time;

public class ChatMessage {

	private static Integer currentId = 0;
	
	private int senderId;
	private String senderName;
	private String message;
	private String date;
	private ChatType chatType;
	private int id;
	
	public ChatMessage(int senderId, String senderName, String message, String date, ChatType chatType) {
		this.senderId = senderId;
		this.senderName = senderName;
		this.message = message;
		this.date = date;
		synchronized (currentId) {
			this.id = currentId;
			currentId++; 
		}
		this.chatType = chatType;
	}
	
	public ChatMessage(int senderId, String senderName, String message, long time, ChatType chatType) {
		this(senderId, senderName, message, Time.timeString(time), chatType);
	}
	
	public int getId() {
		return id;
	}
	
	public String getSenderName() {
		return senderName;
	}
	
	public void setChatType(ChatType chatType) {
		this.chatType = chatType;
	}
	
	public void log() {
		String query = "INSERT INTO chatlog (account_id, message, time) VALUES ("+senderId+", '"+message.replaceAll("'", "''")+"', '"+(new Timestamp(System.currentTimeMillis()))+"');";
		Request req = new Request(null, query, DatabaseType.Log, (Runnable) null);
		DatabaseManager.sheduleRequest(req);
	}

	public Packet generatePacket() {
		TemplatePacket chatTemplatePacket = new TemplatePacket(TemplateType.Chat_Message);
		chatTemplatePacket.put("$id$", ""+id);
		chatTemplatePacket.put("$name$", senderName);
		chatTemplatePacket.put("$message$", message);
		chatTemplatePacket.put("$message_time$", date);
		chatTemplatePacket.put("$chat_type$", chatType.toString());
		return chatTemplatePacket;
	}
	
}
