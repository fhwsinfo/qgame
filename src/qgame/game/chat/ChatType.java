package qgame.game.chat;

import java.util.HashMap;
import java.util.Map;

public enum ChatType {

	Admin("chat_type_admin"),
	User("chat_type_user");
	
	private static Map<String, ChatType> chatTypes;
	
	static {
		chatTypes = new HashMap<String, ChatType>();
		for (ChatType type : ChatType.values()) {
			chatTypes.put(type.toString(), type);
		}
	}
	
	public static ChatType getChatType(String name) {
		return chatTypes.get(name);
	}
	
	private String chatTypeString;
	
	ChatType(String chatTypeString) {
		this.chatTypeString = chatTypeString;
	}
	
	@Override
	public String toString() {
		return chatTypeString;
	}
	
}
