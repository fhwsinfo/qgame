package qgame.game.chat;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import qgame.log.Logger;
import qgame.session.PlayerSession;

public class ChatBuffer {

	private static List<ChatMessage> chatBuffer = new ArrayList<ChatMessage>();
	private static final int maxMessages = 20;
	
	public static ChatMessage addMessage(ChatMessage msg) {
		synchronized (chatBuffer) {
			chatBuffer.add(msg);
			if (chatBuffer.size() > maxMessages) {
				return (chatBuffer.remove(0));
			}
			return null;
		}
	}
	
	public static void send(PlayerSession pSess) {
		synchronized (chatBuffer) {
			for (ChatMessage msg : chatBuffer) {
				pSess.sendChatMessage(msg);
			}
		}
	}
	
	public static void setChatType(String user, ChatType chatType) {
		if (chatType == null)
			chatType = ChatType.User;
		for (ChatMessage msg : chatBuffer) {
			if (msg.getSenderName().equalsIgnoreCase(user))
				msg.setChatType(chatType);
		}
	}
	
	public static void init(ResultSet result) {
		String userName;
		int userId;
		long time;
		String message;
		List<String> users = new ArrayList<String>();
		try {
			while (result.next()) {
				time = result.getTimestamp("time").getTime();
				userName = result.getString("login");
				userId = result.getInt("id");
				message = result.getString("message");
				ChatType chatType = ChatType.User;
				addMessage(new ChatMessage(userId, userName, message, time, chatType));
				if (!users.contains(userName.toLowerCase()))
					users.add(userName.toLowerCase());
			}
			for (String user : users) {
				PlayerSession.requestOfflinePlayer(user);
			}
		} catch (Exception e) {
			Logger.error("Exeption in ChatBuffer.init :");
			Logger.error(e.toString());
		}
	}
	
}
