package qgame.console.command;

import java.util.ArrayList;
import java.util.List;

import qgame.QGame;
import qgame.session.PlayerSession;

public class CommandHandler {

	public static String handleCommand(String line, PlayerSession pSess) {
		line = line.trim();
		String[] split = line.split(" ", 2);
		String[] args;
		int argCount = 0;
		List<String> checkedArgs = new ArrayList<String>();
		String cmd = split[0].toLowerCase();
		if (split.length > 1)
			args = split[1].split(" ");
		else 
			args = new String[0];
		
		if (pSess != null) {
			if (!pSess.hasPermission("COMMAND:EXECUTE_"+cmd.toUpperCase()))
				return "No Permission!";
		}
		switch (cmd) {
			case "shutdown":
				for (String arg : args) {
					if (arg != null && !arg.equals(""))
						return "Too many arguments!";
				}
					
				QGame.shutdown();
				return "";
			case "kick":
				for (String arg : args) {
					if (arg != null && !arg.trim().equals("")) {
						checkedArgs.add(arg);
						argCount++;
					}
					if (argCount != 1) {
						return "Incorrect ammount of arguments!";
					}
				}
				return handleKickCommand(checkedArgs.get(0));
			default:
				return "Not a command!";
		}
		
	}
	
	public static String handleKickCommand(String player) {
		PlayerSession pSess = PlayerSession.getSession(player);
		if (pSess == null)
			return "Player not found!";
		if (!pSess.isOnlineSession())
			return "Player not online!";
		pSess.destroy();
		return "Player kicked.";
	}
	
}
