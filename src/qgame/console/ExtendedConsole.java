package qgame.console;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public abstract class ExtendedConsole extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1393607936764594907L;
	
	protected JTextField input;
	protected JTextArea output;
	protected JScrollPane outputDisplay;
	
	public ExtendedConsole(String name) {
		setTitle(name);
		setSize(400, 600);
		setMinimumSize(new Dimension(400,600));
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE); 
		
		Font font = new Font("Verdana", Font.PLAIN, 10);
		
		output = new JTextArea();
		output.setFont(font);
		output.setEditable(false);
		output.setLineWrap(true);
		output.setWrapStyleWord(true);
		outputDisplay = new JScrollPane(output,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		input = new JTextField();
		input.addActionListener(this);
		
		setLayout(new GridBagLayout());
		
		GridBagConstraints c = new GridBagConstraints();
	    c.gridwidth = GridBagConstraints.REMAINDER;

	    c.fill = GridBagConstraints.BOTH;
	    c.weightx = 1.0;
	    c.weighty = 1.0;
	    add(outputDisplay, c);
	    
	    c.fill = GridBagConstraints.HORIZONTAL;
	    c.weighty = 0.0;
	    add(input, c);
	    
	    pack();
	    setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(input)) {
			HandleInput(input.getText());
			input.setText("");
		}
	}
	
	public void writeLine(String line) {
		if (!line.endsWith("\n"))
			line += "\n";
		output.append(line);
		output.setCaretPosition(output.getDocument().getLength());
	}
	
	public abstract void HandleInput(String line);
	
}