package qgame.console;

public class ConsoleMessage {
	
	private String time;
	private final String message;
	private final String logType;
	
	public ConsoleMessage(String time, String message, String logType) {
		this.time = time;
		this.message = message;
		this.logType = logType;
	}
	
	public ConsoleMessage(String time, String message) {
		this(time, message, "");
	}
	
	public String getLogType() {
		return logType;
	}
	
	public String getTime() {
		return time;
	}
	
	public void setTime(String time) {
		this.time = time;
	}
	
	public String getMessage() {
		return message;
	}
	
}
