package qgame.console;

import java.awt.Color;

import qgame.console.command.CommandHandler;
import qgame.log.Logger;

/**
 * 
 * A simple console for debug output and command input.
 * TODO RemoteConsole implementation
 * 
 * @author Kagamul
 *
 */
public class QGameConsole extends ExtendedConsole  {

	private static final long serialVersionUID = 7244197571567099262L;

	public static boolean consoleOpen = false;
	private static QGameConsole instance;
	
	public static void println(String line) {
		instance.writeLine(line);
	}
	
	public QGameConsole() {
		super("QGame Console");
		instance = this;
		Color c = new Color(0,0,0,255);
        output.setBackground(c);
        c = new Color(0,255,0,255);
        output.setForeground(c);
        consoleOpen = true;
	}

	@Override
	public void HandleInput(String arg0) {
		String result = CommandHandler.handleCommand(arg0, null);
		if (result != "")
			Logger.info(result);
	}

}
