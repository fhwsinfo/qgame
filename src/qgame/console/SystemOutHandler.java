package qgame.console;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

import qgame.log.Logger;

public class SystemOutHandler extends OutputStream {

	private String line = "";
	@Override
	public void write(int b) throws IOException {
		byte[] arr = new byte[1];
    	arr[0] = (byte) b;
    	String add = new String(arr, StandardCharsets.UTF_8);
    	if (add != null)
    		line += add;
    	if (add.equalsIgnoreCase("\n")) {
    		Logger.info(line);
    		line = "";
    	}
		
	}

}
