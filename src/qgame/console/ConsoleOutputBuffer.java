package qgame.console;

import java.util.ArrayList;
import java.util.List;

import qgame.filter.InputFilter;
import qgame.filter.JsonFilter;
import qgame.network.protocol.TemplateType;
import qgame.network.protocol.packets.TemplatePacket;
import qgame.session.PlayerSession;

public class ConsoleOutputBuffer {

	private static List<PlayerSession> listeners = new ArrayList<PlayerSession>();
	
	private static List<ConsoleMessage> buffer = new ArrayList<ConsoleMessage>();
	
	private static TemplatePacket templatePacket = new TemplatePacket(TemplateType.Console_Message);
	
	private static String lastTime = "";
	private static String lastLogType = "";
	
	public static void removeListener(PlayerSession pSess) {
		synchronized (listeners) {
			listeners.remove(pSess);
		}
	}
	
	public static void addListener(PlayerSession pSess) {
		
		synchronized (buffer) {
			for (ConsoleMessage msg : buffer) {
				templatePacket.put("$time$", msg.getTime());
				templatePacket.put("$message$", msg.getMessage());
				templatePacket.put("$logtype$", msg.getLogType());
				pSess.send(templatePacket);
			}
		}
		
		synchronized (listeners) {
			listeners.add(pSess);
		}
	}
	
	public static void addLine(String time, String line) {
		addLine(time, line);
	}
	
	public static void addLine(String time, String line, String logType) {
		line = InputFilter.apply(JsonFilter.apply(line));
		ConsoleMessage msg = new ConsoleMessage(time, line, logType);
		synchronized (buffer) {
			if (lastTime.equals(time) && lastLogType.equals(msg.getLogType()))
				msg.setTime("");
			else {
				lastTime = time;
				lastLogType = msg.getLogType();
			}
			buffer.add(msg);
			templatePacket.put("$time$", msg.getTime());
			templatePacket.put("$message$", msg.getMessage());
			templatePacket.put("$logtype$", msg.getLogType());
			synchronized (listeners) {
				for (PlayerSession pSess : listeners) {
					pSess.send(templatePacket);
				}
			}
		}
		
	}
	
}
