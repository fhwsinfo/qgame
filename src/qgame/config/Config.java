package qgame.config;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import qgame.log.Logger;
import qgame.tools.Pair;

public class Config {

	private final Map<String, Pair<Class<?>, Object>> configValues = new HashMap<String, Pair<Class<?>, Object>>();
	
	private final String filepath;
	
	public Config(String filepath) {
		this.filepath = filepath;
	}
	
	public void read() {
		String line;
		String[] lineSplit;
		try {
			BufferedReader br = new BufferedReader(new FileReader(filepath));
			try {
				while ((line = br.readLine()) != null) {
					lineSplit = line.split("=", 2);
					if (lineSplit.length != 2)
						continue;
					set(lineSplit[0].trim(), lineSplit[1].trim());
				}
			} catch (IOException e) {
				Logger.error("IOException while parsing config file '"+filepath+"':");
				Logger.error(e.toString());
			} finally {
				br.close();
			}
		} catch (FileNotFoundException e) {
			Logger.error("Config file '"+filepath+"' not found.");
			return;
		} catch (IOException e) {
			Logger.error("IOException after parsing config file '"+filepath+"'.");
		}
	}
	
	public void write() {
		read();
		File file = new File(filepath);
		BufferedWriter bw;
		try {
			FileOutputStream fos = new FileOutputStream(file);
			bw = new BufferedWriter(new OutputStreamWriter(fos));
		} catch (FileNotFoundException e) {
			Logger.error("Exception while creating config file '"+filepath+"':");
			Logger.error(e.toString());
			return;
		}
		
		;
		Pair<Class<?>, Object> value;
		String key;
		
		try {
			for (Entry<String, Pair<Class<?>, Object>> entry : configValues.entrySet()) {
				key = entry.getKey();
				value = entry.getValue();
				bw.write(key +" = "+ value.second.toString());
				bw.newLine();
			}
		} catch (IOException e) {
			Logger.error("Exception while writing to config file '"+filepath+"':");
		} finally {
			try {
				bw.close();
			} catch (IOException e) {
				Logger.error("Exception after writing to config file '"+filepath+"':");
				Logger.error(e.toString());
			}
		}
		
	}
	
	
	public void set(String key, int value) {
		configValues.put(key.toLowerCase(), new Pair<Class<?>, Object>(Integer.class, value));
	}
	
	public void set(String key, String value) {
		configValues.put(key.toLowerCase(), new Pair<Class<?>, Object>(String.class, value));
	}
	
	
	
	public int get(String key, int defaultValue, int minValue, int maxValue) {
		Pair<Class<?>, Object> configValuesEntry;
		Integer value = null;
		
		configValuesEntry = configValues.get(key.toLowerCase());
		
		if (configValuesEntry != null) {
			if (configValuesEntry.first == Integer.class) {
				value = (Integer) configValuesEntry.second;
			}
			if (configValuesEntry.first == String.class) {
				try {
					value = Integer.parseInt((String) configValuesEntry.second);
				} catch (NumberFormatException e) { 
					value = null; 
					Logger.error("Config entry '"+key+"' = '"+(String) configValuesEntry.second+"' is not an Integer!");
				}
			}
			if (value != null) {
				if (value >= minValue && value <= maxValue)
					return value;
				Logger.error("Config entry '"+key+"' was "+value+" but has to be in range "+minValue+" >= value >= "+maxValue+"!");
				
			}
		}
		set(key, defaultValue);
		return defaultValue;
	}
	
	public String get(String key, String defaultValue, ConfigStringValidator validator) {
		Pair<Class<?>, Object> configValuesEntry;
		String value;
		
		configValuesEntry = configValues.get(key.toLowerCase());
		
		if (configValuesEntry != null) {
			value = configValuesEntry.second.toString();
			if (validator.validate(value))
				return value;
		} else {
			set(key, defaultValue);
		}
		return defaultValue;
	}
	
}
