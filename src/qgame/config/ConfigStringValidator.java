package qgame.config;

public interface ConfigStringValidator {

	public boolean validate(String str);
	
}
