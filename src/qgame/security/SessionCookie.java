package qgame.security;

import java.security.SecureRandom;

public class SessionCookie {

	public static int generateRandomInt() {
		SecureRandom r = new SecureRandom();
		return r.nextInt();
	}
	
	public static int generateStoreCookie(int randInt, int ipHash) {
		return randInt ^ ipHash;
	}
	
	public static boolean checkCookie(int clientCookie, int storeCookie, int ipHash) {
		return generateStoreCookie(clientCookie, ipHash) == storeCookie;
	}
	
}
