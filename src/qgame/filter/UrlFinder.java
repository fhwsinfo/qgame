package qgame.filter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * A filter that searches a String for URLs and replaces them with html links
 * 
 * @author Kagamul
 * 
 */
public class UrlFinder {

	private static final Pattern urlPattern = Pattern.compile(
	        "(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)"
	                + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
	                + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
	        Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
	
	public static String parseUrls(String in) {
		String out = "";
		String url = "";
		int ind = 0;
		Matcher matcher = urlPattern.matcher(in);
		while (matcher.find()) {
			int matchStart = matcher.start(1);
			int matchEnd = matcher.end();
			out += in.substring(ind, matchStart);
			url = in.substring(matchStart, matchEnd);
			out += createUrl(url);
			ind = matchEnd;
		}
		return out + in.substring(ind, in.length());
	}
	
	private static String createUrl(String url) {
		url = url.replace("\\", "/");
		if (url.length() < 26)
			if (url.toLowerCase().startsWith("www."))
				return "<a href='http://"+url+"' target='_blank'>"+url+"</a>";
			else
				return "<a href='"+url+"' target='_blank'>"+url+"</a>";
		int pos = 0;
		if (url.toLowerCase().startsWith("www.")) {
			pos = url.indexOf("/");
			if (pos > 0 && pos < 24) {
				return "<a href='http://"+url+"' target='_blank'>"+url.substring(4, pos)+"/...</a>";
			}
			return "<a href='http://"+url+"' target='_blank'>"+url.substring(4, 24)+"...</a>";
		}
		pos = url.indexOf("//");
		if (pos == -1)
			return "";
		int cut = pos+2;
		pos = url.indexOf("/", cut);
		if (pos == -1)
			pos = url.length();
		int diff = pos - cut;
		if (diff > 2 && diff < 22)
			return "<a href='"+url+"' target='_blank'>"+url.substring(cut, pos)+"/...</a>";
		return "<a href='"+url+"' target='_blank'>"+url.substring(cut, cut+20)+"[...]</a>";
	}
	
}
