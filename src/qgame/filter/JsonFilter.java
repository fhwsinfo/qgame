package qgame.filter;

/**
 * 
 * A filter that replaces Keys from an String with predefined values
 * JsonFilter is applied to all inbound json messages.
 * It ensures that backslashes are handled properly and that quotationmarks and semicolons are displayed correctly
 * without this the json interpreter might have issues parsing the String, as backslashes are seen as escape characters.
 * 
 * @author Kagamul
 * 
 */
public enum JsonFilter {
	NoSemicolon("\\;","&#59;"),
	QuotFix("&quot&#59;","&quot;"),
	NoBackslash("\\\\","&#92;");
	
	
	
	public static String apply(String in) {
		String out = in.trim();
		for (JsonFilter filter : JsonFilter.values())
			out = out.replaceAll(filter.getKey(), filter.getValue());
		return out;
	}
	
	private String key;
	private String value;
	
	JsonFilter(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	public String getKey() {
		return key;
	}
	
	public String getValue() {
		return value;
	}
}
