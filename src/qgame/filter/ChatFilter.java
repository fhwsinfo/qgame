package qgame.filter;

/**
 * 
 * A filter that replaces Keys from an String with predefined values
 * ChatFilter is applied to the chat_message of a client only.
 * It enables emoticons and could be used for censoring purposes.
 * 
 * @author Kagamul
 * 
 */
public enum ChatFilter {

	PreChatFilter("://", "<lnk>"),
	
	Censored(":censored:", "<img src='img/chat/censored.gif' alt=':censored:'/>"),
	LoughingMan(":lman:", "<img src='img/chat/lman.png'/>"),
	Cat(":3", "<img src='img/chat/ChatCat.png' title=':3' />"),
	Happy(":D", "<img src='img/chat/emots/Happy.png' title=':D'/>"),
	Sad(":&#40;", "<img src='img/chat/emots/Sad.png' title=':-('/>"),
	Worried(":/", "<img src='img/chat/emots/Worried.png' title=':/'/>"),
	Wink("&#59;&#41;", "<img src='img/chat/emots/Wink.png' title=';)'/>"),
	//Wondering("O_o", "<img src='img/chat/emots/' title='O_o'/>"),
	NotSure(":notsure:", "<img src='img/chat/emots/NotSure.png' title=':notsure:'/>"),
	
	PostChatFilter("<lnk>", "://");

	
	public static String apply(String in) {
		String out = in;
		for (ChatFilter filter : ChatFilter.values())
			out = out.replaceAll(filter.getKey(), filter.getValue());
		return out;
	}
	
	private String key;
	private String value;
	
	ChatFilter(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	public String getKey() {
		return key;
	}
	
	public String getValue() {
		return value;
	}
	
}
