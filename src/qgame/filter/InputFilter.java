package qgame.filter;

/**
 * 
 * A filter that replaces Keys from an String with predefined values
 * InputFilter is applied to any user sent field to prevent injections and other abuse. 
 * 
 * @author Kagamul
 * 
 */
public enum InputFilter {

	NoBackslash("\\\\","&#92;"),
	NoLT("<", "&lt;"),
	NoGT(">", "&gt;"),
	NoNewline("\n","&#59;n"),
	NoTab("\t","    "),
	NoQuot("\"","&quot;"),
	NoSingleQuot("'","&#39;"),
	NoOpenBracket("\\(", "&#40;"),
	NoCloseBracket("\\)", "&#41;");
	
	
	
	public static String apply(String in) {
		String out = in.trim();
		for (InputFilter filter : InputFilter.values())
			out = out.replaceAll(filter.getKey(), filter.getValue());
		return out;
	}
	
	private String key;
	private String value;
	
	InputFilter(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	public String getKey() {
		return key;
	}
	
	public String getValue() {
		return value;
	}
	
}
