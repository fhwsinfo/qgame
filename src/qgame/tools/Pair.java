package qgame.tools;

public class Pair<A, B> {

	public A first;
	public B second;
	
	public Pair(A a, B b) {
		first = a;
		second = b;
	}
	
	@Override
	public int hashCode() {
		return first.hashCode() * second.hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Pair<?,?>))
			return false;
		
		Pair<?,?> p = (Pair<?,?>) o;
		
		if (!p.first.equals(first)) {
			return false;
		}
		if (!p.second.equals(second)) {
			return false;
		}
		return true;
	}
	
}
