package qgame.tools;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.json.simple.parser.ContainerFactory;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JSON {

	private static final ContainerFactory containerFactory = new ContainerFactory() {

		@Override
		public List<String> creatArrayContainer() {
			return new LinkedList<String>();
		}

		@Override
		public Map<String, String> createObjectContainer() {
			return new LinkedHashMap<String, String>();
		}
		
	};
	
	@SuppressWarnings("unchecked")
	public static Map<String, String> parseJson(String jsonString) throws ParseException {
		JSONParser parser = new JSONParser();
		return (Map<String, String>) parser.parse(jsonString, containerFactory);
	}
	
}
