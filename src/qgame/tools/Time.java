package qgame.tools;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

public class Time {

	private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM HH:mm:ss", new Locale("de"));
	
	public static String currentTimeString() {
		return LocalDateTime.now().format(formatter);
	}
	
	public static String timeString(long time) {
		long diff = System.currentTimeMillis() - time;
		return LocalDateTime.now().minus(diff, ChronoUnit.MILLIS).format(formatter);
	}
	
}