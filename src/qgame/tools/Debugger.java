package qgame.tools;

import java.util.Map;
import java.util.Map.Entry;

import qgame.log.Logger;

public class Debugger {
	
	public static void Debug(Map<?,?> map) {
		for (Entry<?, ?> entry : map.entrySet()) {
			Logger.debug(entry.getKey().toString() +" --> "+entry.getValue().toString());
		}
	}
	
}
