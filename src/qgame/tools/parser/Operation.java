package qgame.tools.parser;

//http://www.java-forum.org/allgemeines/12306-parser-fuer-mathematische-formeln.html

public interface Operation{
   public int getPriority();

   public double calculate( double a, double b );
}