package qgame.tools.parser;
// http://www.java-forum.org/allgemeines/12306-parser-fuer-mathematische-formeln.html

public interface Function {

    public boolean validNrOfArguments( int count );

    public double calculate( double[] values );
}